export const polo_man = {
    'model_path': '/models/polo_man/polo_man.glb',
    'parts': [
        { name: "frente",              x:  0.0,  y: 0.3,  z:  2.8 , text: 'Frente', embroideries: true, includedPartsNames: null},
        { name: "espalda",             x:  0.2,  y: 0.5,  z: -2.9, text: 'Espalda', embroideries: true, includedPartsNames: null},
        { name: "mangas",            x: -2.5,  y: 0.5,  z:  0.2, text: 'Mangas', embroideries: false, includedPartsNames: ["manga_izquierda", "manga_derecha"]},
        // { name: "manga_izquierda",     x: 2.5,   y: 0.5,  z:  0.2},
        // { name: "manga_derecha",       x: -2.5,  y: 0.5,  z:  0.2},
        { name: "cuello",              x: 0.13,  y:   2,  z:    2, text: 'Cuello', embroideries: true, includedPartsNames: null},
        { name: "base_mangas",        x: -2.3,  y:-0.5,  z:-0.15, text: 'Base de las mangas', embroideries: false, includedPartsNames: ["basemanga_izquierda", "basemanga_derecha"]},
        // { name: "basemanga_izquierda", x: 2.3,   y:-0.5,  z:-0.15},
        // { name: "basemanga_derecha",   x: -2.3,  y:-0.5,  z:-0.15},
        { name: "baseboton_atras",     x:  -0.5,  y: 1.7,  z:  1.6, text: 'Base del botón trasero', embroideries: true, includedPartsNames: null},
        { name: "baseboton_delante",   x:  0.5,  y: 1.7,  z:  1.6, text: 'Base del botón delantero', embroideries: true, includedPartsNames: null}
    ],
    'clothes': {
        'frente': [
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/frente/texture_frente0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente1.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente2.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente3.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },					
					{ color: '64739B' },
					{ color: 'CDBAC7' },
					{ color: '946F43' },
					{ color: '66533C' },
					{ color: '286f85' },
					{ color: '27548D' },
					{ color: '438AAC' },
					{ color: 'de3d35' },
					{ color: 'ecf54c' },
					{ color: '3d3d3d' },
					{ color: '0c2d19' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente4.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },					
				]
			},
        ],
        'espalda': [
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/espalda/texture_espalda0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/espalda/texture_espalda1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/espalda/texture_espalda2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/espalda/texture_espalda3.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'mangas': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/manga/texture_manga0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/manga/texture_manga1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
        ],
        'cuello': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/cuello/texture_cuello0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/cuello/texture_cuello1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/cuello/texture_cuello2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/cuello/texture_cuello3.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/cuello/texture_cuello4.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/cuello/texture_cuello5.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/cuello/texture_cuello6.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'base_mangas': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga3.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga4.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga5.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela 2',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga6.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'baseboton_atras': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Especial 1',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela Especial 2',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'baseboton_delante': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Especial 1',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela Especial 2',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
    }
};