export const polo_man_mangalarga = {
    'model_path': '/models/polo_man_mangalarga/polo_man_mangalarga.glb',
    'parts': [
        { name: "frente",              x:  0.0,  y: 0.3,  z:  2.8 , text: 'Frente', embroideries: true, includedPartsNames: null},
        { name: "espalda",             x:  0.2,  y: 0.5,  z: -2.9, text: 'Espalda', embroideries: true, includedPartsNames: null},
        { name: "mangas",            x: -2.5,  y: 0.5,  z:  0.2, text: 'Mangas', embroideries: false, includedPartsNames: ["manga_izquierda", "manga_derecha"]},
        // { name: "manga_izquierda",     x: 2.5,   y: 0.5,  z:  0.2},
        // { name: "manga_derecha",       x: -2.5,  y: 0.5,  z:  0.2},
        { name: "cuello",              x: 0.13,  y:   2,  z:    2, text: 'Cuello', embroideries: true, includedPartsNames: null},
        { name: "puños",        x: -2.3,  y:-0.5,  z:-0.15, text: 'Puños', embroideries: false, includedPartsNames: ["puño_derecha", "puño_izquierda"]},
        // { name: "basemanga_izquierda", x: 2.3,   y:-0.5,  z:-0.15},
        // { name: "basemanga_derecha",   x: -2.3,  y:-0.5,  z:-0.15},
        { name: "perilla_puños",        x: -2.3,  y:-0.5,  z:-0.15, text: 'Perilla de los puños', embroideries: false, includedPartsNames: ["perillapuño_derecha", "perillapuño_izquierda"]},
        { name: "botonadura",     x:  0.0,  y: 0.3,  z:  2.8, text: 'Botonadura', embroideries: false, includedPartsNames: null},
        { name: "tapaojal",   x:  0.0,  y: 0.3,  z:  2.8, text: 'Tapa ojal', embroideries: false, includedPartsNames: null},
        { name: "botones",   x:  0.0,  y: 0.3,  z:  2.8, text: 'Botones', embroideries: false, includedPartsNames: null},
    ],
    'clothes': {
        'frente': [
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/frente/texture_frente0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente1.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente2.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente3.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },					
					{ color: '64739B' },
					{ color: 'CDBAC7' },
					{ color: '946F43' },
					{ color: '66533C' },
					{ color: '286f85' },
					{ color: '27548D' },
					{ color: '438AAC' },
					{ color: 'de3d35' },
					{ color: 'ecf54c' },
					{ color: '3d3d3d' },
					{ color: '0c2d19' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente4.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },					
				]
			},
        ],
        'espalda': [
            {
                name: 'Lino',
                texture: 'models/polo_man/textures/frente/texture_frente0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente1.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente2.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente3.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },					
					{ color: '64739B' },
					{ color: 'CDBAC7' },
					{ color: '946F43' },
					{ color: '66533C' },
					{ color: '286f85' },
					{ color: '27548D' },
					{ color: '438AAC' },
					{ color: 'de3d35' },
					{ color: 'ecf54c' },
					{ color: '3d3d3d' },
					{ color: '0c2d19' },
				]
			},
			{
				name: 'Algodón 50%',
				texture: 'models/polo_man/textures/frente/texture_frente4.jpg',
				size: [6, 6, 6],
				shininess: 0,
				colors: [
					{ color: 'ffffff' },					
				]
			},				
        ],
        'mangas': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/manga/texture_manga0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/manga/texture_manga1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
        ],
        'cuello': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/cuello/texture_cuello0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            }
        ],
        'puños': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/basemanga/texture_basemanga0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
        ],
        'perilla_puños': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Especial 1',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela Especial 2',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'botonadura': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Especial 1',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela Especial 2',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'tapaojal': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Especial 1',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela Especial 2',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
        'botones': [
            {
                name: 'Tela Blanca',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton0.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                    { color: '64739B' },
                    { color: 'CDBAC7' },
                    { color: '946F43' },
                    { color: '66533C' },
                    { color: '286f85' },
                    { color: '27548D' },
                    { color: '438AAC' },
                    { color: 'de3d35' },
                    { color: 'ecf54c' },
                    { color: '3d3d3d' },
                    { color: '0c2d19' },
                ]
            },
            {
                name: 'Tela Especial 1',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton1.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
            {
                name: 'Tela Especial 2',
                texture: 'models/polo_man/textures/baseboton/texture_baseboton2.jpg',
                size: [6, 6, 6],
                shininess: 0,
                colors: [
                    { color: 'ffffff' },
                ]
            },
        ],
    }
};