// Importing outils's functions
'use strict';

import { Environment } from './Environment.js';
// import { buildPartsCarrousel } from  "./others/view-builder.js";

import { polo_man } from  "../models/polo_man/config_file.js";
import { polo_woman } from  "../models/polo_woman/config_file.js";
import { polo_man_mangalarga } from  "../models/polo_man_mangalarga/config_file.js";

/* ********************** PRINCIPAL FUNCTIONS **************************** */

// const MODEL_PATH = "./models/polo/polo.glb";
db = {
    'models': {
        polo_woman,
        polo_man,
        polo_man_mangalarga,
    }
}

// Activan este cuando quieran probar alguno de los dos modelos -> "polo_man" / "polo_woman"
// let modelName = "polo_man"

// Activan este cuando quieran que se coloque el modelo dependiendo el elemento activo en el carousel,
// falta arreglar para que se mueva dinamico
// let modelName = $('#carouselModels li.active').attr('data-option');
// const MODEL_PATH = "./models/"+ modelName +"/"+ modelName +".glb";

// env = new Environment(MODEL_PATH, modelName);

// $(function() {
//     env.init("#c");
// });

$(function() {
    $('#selectModelButton').on('click', function() {
        let modelName = $('#carouselModels li.active').attr('data-option');
        const MODEL_PATH = "./models/"+ modelName +"/"+ modelName +".glb";
        env = new Environment(MODEL_PATH, modelName);
        env.init("#c");
        // buildPartsCarrousel();
    });
});

/* ********************** ADITIONAL FUNCTIONS **************************** */

window.onbeforeunload = function(event) {
    event.returnValue = "Está seguro que quiere abandonar la página? El modelo no ha sido guardado.";
    env.dispose();
};