$(function() {
	// buildPartsCarrousel();

	$('[data-modal="modal"]').on('click', function() {
		$($(this).attr('data-target')).modal('show');
	});

	// Carrousel
	$('#partsCarrousel').on('click', 'li', function() {
		env.setSelectedEmbroidery(-1);
		env.updateEmbroideries();
		$('#partsCarrousel li.active').removeClass('active');
		$(this).addClass('active');
		env.updateBuildTexturesAndColors();
		if($('#animateCheckbox:checked').val() != undefined)
			env.rotateTo( $(this).attr('data-option') );
	});

	$('body').on('change', '#rotateCheckbox', function() {
		let val = $(this).is(':checked');
		env.setAutoRotate(val);
	});

	$('body').on('click', '.nav-item', function() {
		$('.nav-item').removeClass('active');
		$(this).addClass('active');
	});
});

// export function buildPartsCarrousel() {
// 	let $container = $('#partsCarrousel');
// 	$container.html('');

// 	// let currentModel = 'polo_man';
// 	let currentModel = $('#carouselModels li.active').attr('data-option');
	
// 	let row = db['models'][currentModel];
// 	let $li, $img, $span, $text;

// 	for (let i = 0; i < row['parts'].length; i++) {

// 		// I will create the image option just if that part accepts embroideries
// 		if (row['parts'][i]['embroideries']) {
// 			$img = $('<span>', {
// 				'class': 'img',
// 				'style': 'background-image: url("models/'+currentModel+'/parts/'+row['parts'][i]['name']+'.png");'
// 			});
// 			$span = $('<span>', {
// 				'class': 'decoration-selected'
// 			});
// 			$li = $('<li>', {
// 				'data-option': row['parts'][i]['name']
// 			});
// 			$text = $('<p>', {
// 				'class': 'font-weight-bold text-secondary',
// 				'style': 'letter-spacing: 2px;',
// 				'text': row['parts'][i]['text'],
// 			});
// 			$li.append($img);
// 			$li.append($span);
// 			$li.append($text);
// 			$container.append($li);
// 		}
// 	}
// 	$('#partsCarrousel li:first-child').addClass('active');
// }