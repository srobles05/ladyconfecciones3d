function notify(type, msg) {
    $.notify({
        message: msg 
    },{
        type: type,
        animate: {
            enter: 'animate__animated animate__fadeInDown',
            exit: 'animate__animated animate__fadeOutUp'
        },
    });
}