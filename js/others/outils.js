export function getCorner(pivotX, pivotY, cornerX, cornerY, angle) {
    let x, y, distance, diffX, diffY;

    /// get distance from center to point
    diffX = cornerX - pivotX;
    diffY = cornerY - pivotY;
    distance = Math.sqrt(diffX * diffX + diffY * diffY);

    /// find angle from pivot to corner
    angle += Math.atan2(diffY, diffX);

    /// get new x and y and round it off to integer
    x = pivotX + distance * Math.cos(angle);
    y = pivotY + distance * Math.sin(angle);

    return {x:x, y:y};
}

export function getBoxSizes(w, h, angle) {
    if((angle > Math.PI * 0.5 && angle < Math.PI * 1) || (angle > Math.PI * 1.5 && angle < Math.PI * 2))
        angle = Math.PI - angle;

    let ww = Math.abs(Math.sin(angle) * h + Math.cos(angle) * w);
    let hh = Math.abs(Math.sin(angle) * w + Math.cos(angle) * h);

    return {w:ww, h:hh};
}

/**
 * To set initial rotation when the model is initially loaded
 * @param {number} initRotate - A number usually initializated with 0 but it should be lower than 120.
 * @param {Threejs Object} model - 3D loaded model.
 */
export function initialRotation(initRotate, model) {
    initRotate++;

    if (initRotate <= 120) {
        model.rotation.y += Math.PI / 60;
    } else {
        return [initRotate, true];
    }

    return [initRotate, false];
}

/**
 * A resizing method for the navigator.
 * @param {Threejs Object} renderer - A Threejs Object with the render options.
 */
export function resizeRendererToDisplaySize(renderer, force=false) {
    const canvas = renderer.domElement;
    var width = window.innerWidth;
    var height = window.innerHeight;
    var canvasPixelWidth = canvas.width / window.devicePixelRatio;
    var canvasPixelHeight = canvas.height / window.devicePixelRatio;

    const needResize = canvasPixelWidth !== width || canvasPixelHeight !== height;

    if (needResize || force) {
        renderer.setSize(width, height, false);
    }

    return needResize;
}

/**
 * A notification.
 * @param {string} type - (danger, warning, success, info)
 * @param {string} msg - The message you wanna show
 */
export function notify(type, msg) {
    $.notify({
        message: msg
    },{
        type: type,
        animate: {
            enter: 'animate__animated animate__fadeInDown',
            exit: 'animate__animated animate__fadeOutUp'
        },
    });
}

/**
 * Returns true if the part is available for embroideries, false if it is not.
 * @param {Array} parts - List of available parts for a model
 * @param {String} partName - Name of the selected part.
 */
export function getPartEmbroideryAvailability(parts, partName) {
    let availability = true;
    
    for (let index = 0; index < parts.length; index++) {
        const part = parts[index];

        // Traduction section when we have multiple sections included in one -> We pass from the specific section to the general one
        if (part.includedPartsNames != null) {

            for (let includedPartsIndex = 0; includedPartsIndex < part.includedPartsNames.length; includedPartsIndex++) {
                const includedPartName = part.includedPartsNames[includedPartsIndex];
                
                if (partName == includedPartName) {
                    partName = part.name;
                    break;
                }
            }
        }
        
        // We check if the section is available for embroideries
        if (part.name == partName) {
            if (!part.embroideries) {
                availability = false;
            }
            break;
        }
    }

    return availability;
}