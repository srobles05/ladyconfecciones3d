$(function() {
	// For testing
	activateView(1);
	// activeTab(1);

	$('[data-toggle="tooltip"]').tooltip({'delay': { show: 500, hide: 200 }});

	$('#selectModelButton').on('click', function() {
		activateView(2);
		setTimeout(function() {
			let w = $('#canvasContainer').width();
	        let h = $('#canvasContainer').height();
	        env.renderer.setSize(w, h);
		}, 250);

		// let val = $('.gallery-cell.is-selected').attr('data-name');
		// env = new Environment(db['models'][val]['model_path']);
		// $(function() {
		// 	env.init("#c");
		// });
	});

	$('#backSelectModel').on('click', function() {
		activateView(1);
	});

	// $('.model-item').on('click', function() {
	// 	$('.model-item').removeClass('active');
	// 	$(this).addClass('active');
	// });

	$('body').on('click', '#resetModel', function() {
		env.reset();
		$('.stamps-box .card:not(.add-card)').remove();
		$('#resetModelModal').modal('hide');
	});

	$('body').on('click', '#takeImages', function() {
		env.takeImages();
		$('#takeImagesModal').modal('hide');
	});

	$('body').on('click', '#newTextStamp', function() {
		openTextModal('add');
	});

	$('#addImageButton').on('click', function() {
		let file = document.querySelector('#imgField').files[0];
		let reader = new FileReader();
		let currentSection = $('#partsCarrousel li.active').attr('data-option');

		reader.onloadend = function () {
			if(reader.result == undefined || reader.result == null || reader.result == "") {
				notify('warning', 'Debe ingresar una imagen válida');
				return;
			}
			let data = {
				'value': reader.result
			};
			let img = new Image();
			img.onload = function() {
				placeholderStamp(true);
				let data2 = {
					'img': img
				};
				ind = env.newPlaceholderStamp(currentSection, data2, placeholderStamp);
				newRow('#logo', data, ind, true);
			};
			img.src = reader.result;
		}

		if(file) {
			reader.readAsDataURL(file);
		}
	});

	$('#addTextButton').on('click', function() {
		let text = $('[name="add_text_field"]').val();
		let type = $('[name="add_type_field"]').val();
		let color = $('[name="add_color_field"]').val();
		let currentSection = $('#partsCarrousel li.active').attr('data-option');
		// console.log(text, type, color);
		let data = {
			'value': text,
			'type': type,
			'color': color
		};

		if(text === "") {
			// alert("(Poner más bonito!) Por favor, introduzca un texto.");
			notify('warning', 'Debe ingresar un texto');
			return;
		}

		let action = $(this).attr('data-action');
		let ind;
		if(action == 'add') {
			placeholderStamp(true);
			let data2 = {
				'textFont': type,
				'textContent': text,
				'textColor': color
			};
			ind = env.newPlaceholderStamp(currentSection, data2, placeholderStamp);
			newRow('#text', data, ind, false);
		} else {
			ind = $('[name="data_index"]').val();
			env.updateEmbroidery(ind, data);
			updateRow('#text', data, ind, false);
		}
	});

	$('body').on('click', '.view-action', function() {
		$(this).toggleClass('active');
		let index = $(this).closest('.card').attr('data-index');
		let data = {
			'visible': !$(this).hasClass('active')
		};
		env.updateEmbroidery(index, data);
	});

	$('body').on('click', '.edit-row-button', function() {
		let index = $(this).closest('.card').attr('data-index');
		let $card = $(this).closest('.card');
		let data = {
			'value': $card.find('.text-value').html(),
			'type': $card.find('.type-value').html(),
			'color': $card.find('.color-value').attr('data-color'),
			'index': index
		};
		console.log(data);
		setValuesTextModal(data);
		openTextModal('edit');
		// env.removeEmbroidery(index);
		// $(this).closest('.card').remove();
		// index = parseInt(index) + 1;
		// while(true) {
		// 	let aux = $('#text .card[data-index='+index+']');
		// 	if(aux.length == 0)
		// 		break;
		// 	aux.attr('data-index', (index-1));
		// 	index += 1;
		// }
	});

	$('body').on('click', '.delete-row-button', function() {
		let index = $(this).closest('.card').attr('data-index');
		env.removeEmbroidery(index);
		// env.removeEmbroidery(index);
		// $(this).closest('.card').remove();
		// index = parseInt(index) + 1;
		// while(true) {
		// 	let aux = $('#text .card[data-index='+index+']');
		// 	if(aux.length == 0)
		// 		break;
		// 	aux.attr('data-index', (index-1));
		// 	index += 1;
		// }
	});

	$('body').on('hidden.bs.modal', '#addTextModal', function(e) {
		env.setSelectedEmbroidery(-1);
		env.updateEmbroideries();
	});
});

function activateView(id) {
	if($('.view[data-id="'+id+'"]').hasClass('active'))
		return;
	$('.view.active').addClass('disabled');
	$('.view[data-id="'+id+'"]').addClass('active');
	setTimeout(function() {
		$('.view.disabled').removeClass('active').removeClass('disabled');
	}, 1000);
}

function activeTab(id) {
	$('#optionsTab .nav-item').removeClass('active');
	$('#optionsTab .nav-item:nth-child('+id+')').addClass('active');
	$('#optionsTab .nav-item:nth-child('+id+') .nav-link').trigger('click');
}

function newRow(container, data, index, isImage) {
	let $card, $card_body, $data, $actions_box;
	// console.log(data);

	$card = $('<div>', {
		'class': 'card placeholder',
		'data-index': index
	});
	$card_body = $('<div>', {
		'class': 'card-body'
	});
	if(isImage) {
		$data = $('<img>', {
			'src': data['value']
		});
	} else {
		$data = $('<div>', {
			'class': 'inline-block'
		});
		let $text = $('<p>', {
			'html': data['value'],
			'class': 'my-0 ml-2 text-value'
		});
		let $color = $('<span>', {
			'html': 'MI',
			'class': 'ml-2 mr-1 color-detail color-value',
			'data-color': data['color'],
			'style': 'color: ' + data['color'] + '; background-color: ' + data['color']
		});
		let $type = $('<span>', {
			'html': data['type'].charAt(0).toUpperCase() + data['type'].slice(1),
			'class': 'my-0 text-capitalize type-value'
		});
		$data.append($text).append($color).append($type);
		// $data.append($aux).append($details);
		// let $details = $('<div>', {
		// 	// 'html': '<span class="color-detail" style="background-color: '+data["color"]+'"></span>' + data['type'],
		// 	// 'class': 'my-0 ml-2 text-capitalize'
		// });
		// let $color = $('<div>', {
		// 	'class': 'color-detail',
		// 	'style': 'background-color: ' + data['color']
		// });
		// let $text = $('<p>', {
		// 	'html': data['type'],
		// 	'class': 'my-0 ml-2 text-capitalize'
		// });
		// $details.append($color).append($text);
		// $data.append($aux).append($details);
	}
	$actions_box = $('<div>', {
		'class': 'actions-box',
		'html': '<span class="edit-row-button"><i class="fa fa-edit fa-lg"></i></span><span class="view-action"><i class="fa fa-eye fa-lg"></i><i class="fa fa-eye-slash fa-lg"></i></span><span class="delete-row-button"><i class="fa fa-trash fa-lg"></i></span>'
	});
	$card_body.append($data).append($actions_box);
	$card.append($card_body);
	$(container+' .card.add-card').before($card);
}

function updateRow(container, data, index, isImage) {
	let aux = container + ' .card[data-index="'+index+'"] ';
	$(aux + '.text-value').html(data['value']);
	$(aux + '.type-value').html(data['type']);
	$(aux + '.color-value').css('color', data['color']).css('background-color', data['color']).attr('data-color', data['color']);
}

function placeholderStamp(action, success) {
	if(success == undefined) success = true;
	if(action) {
		$('#placeholderStampTooltip').addClass('active');
		$('.view.active > .row').addClass('placeholder-stamp-active');

		$('body').on('mousemove', function(e) {
			$('#placeholderStampTooltip').css('top', e.pageY+1).css('left', e.pageX+1);
		});
		$('body').on('keyup', function(e) {
			if(e.keyCode == 27)
				placeholderStamp(false, false);
		});
	} else {
		$('#placeholderStampTooltip').removeClass('active');
		$('.view.active > .row').removeClass('placeholder-stamp-active');
		$('body').off('mousemove').off('keyup');
		env.clearPlaceholderStamp();
		if(!success) {
			$('.card.placeholder').remove();
		} else {
			$('.card.placeholder').removeClass('placeholder');
		}
	}
}

function setValuesTextModal(data) {
	$('[name="data_index"]').val(data['index']);
	$('[name="add_text_field"]').val(data['value']);
	$('[name="add_color_field"]').val(data['color']);
	$('[name="add_type_field"]').val(data['type']);
}

function openTextModal(action) {
	let title = "Agregar texto";
	let successButton = "Agregar";

	if(action == 'edit') {
		title = "Editar texto";
		successButton = "Guardar";
	}
	$('#addTextModal .modal-title').html(title);
	$('#addTextModal #addTextButton').html(successButton);
	$('#addTextModal #addTextButton').attr('data-action', action);
	$('#addTextModal').modal('show');
}