/**
 * @fileoverview A class oriented to describe an image Embroidery on each model's section.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 -- 
 * ----
 * 
*/

import { Embroidery } from "./Embroidery.js";

import {
    getBoxSizes
} from "../others/outils.js";

/**
 * A class for all embroideries that will have text on the model.
 * @class
 */
export class ImageEmbroidery extends Embroidery {

    /**
     * @constructor
     * @extends {embroideries.Embroidery} - Extends from the class Embroidery located at /js/embroideries/Embroidery.js
     * @param {string} modelName - String with the model's name.
     * @param {string} section - String with the model's section name.
     * @param {number} posU - Position taken from the 3D to the 2D projection.
     * @param {number} posV - Position taken from the 3D to the 2D projection.
     */
    constructor(modelName, section, img, handlerImages, posU=0, posV=0) {
        let handlerImagesWithoutEdit = handlerImages.slice();
        handlerImagesWithoutEdit.pop();
        super(section, "ImageEmbroidery", modelName, handlerImagesWithoutEdit, posU, posV);

        this.img = img;
        this.ratio = (this.img.width / this.img.height).toFixed(2);
        this.size = 120;
        this.handlers[2].setScalingFactor(this.size);

        // Getting canvas and working with it
        this.boxEdit = document.createElement('canvas');

        // Getting context
        this.ctxBoxEdit = this.boxEdit.getContext('2d');
    }

    /**
     * 
     * @param {Object} data
     * @returns {void} 
     */
    update(data) {
        if(data['visible'] != undefined) this.setVisible(data['visible']);
        if(data['img'] != undefined) this.setImage(data['img']);
    }

    /**
     * Measures the box's width and height.
     * @returns {void}
     */
    measureBox() {
        // If I am zooming in or out I need to update the text size
        if (this.handlers[2].getZoomState()) {
            this.size = this.handlers[2].getScalingFactor();
        }

        // Setting width and height
        this.setWidthBox(this.size);
        this.setHeightBox(this.size / this.ratio);

        if (!this.editing) {
            // // console.log("METRICS: ", metrics, " H: ", h);
            this.boxEdit.width = this.widthBox + ( this.iconSize * 2 );
            this.boxEdit.height = this.heightBox + ( this.iconSize * 2 );
        }
    }

    /**
     * Draw text in a canvas buffer.
     * @param {canvas} buffer - Canvas buffer.
     * @returns {void}
     */
    draw(buffer) {
        if(!this.getVisible())
            return;

        let ctxBuffer = buffer.getContext('2d');
        this.measureBox();

        // If this embroidery is selected for editing
        this.drawLinesAndCorners(0.08, this.editing);
        if(this.editing) {
            // Drawing handlers
            // context.drawImage(     img,             sx,sy, swidth,sheight,x,  y,                   width,height);

            // When fixedEmbroidery is activated we'll just draw it, otherwise we'll draw all the handlers
            if (this.handlers[1].getFixed()) {

                this.handlers.forEach((handler, idx) => {
                    // Fixed Embroidery position
                    if (idx == 1)
                        this.handlers[1].draw(this.ctxBoxEdit, this.boxEdit.width, this.boxEdit.height, this.iconSize);
                    else
                        handler.setDrawn(false);
                });

            } else {
                this.handlers.forEach(handler => {
                    handler.draw(this.ctxBoxEdit, this.boxEdit.width, this.boxEdit.height, this.iconSize);
                });
            }
        }

        this.drawImg();
        
        // this.setWidthBox(this.boxEdit.width);
        // this.setHeightBox(this.boxEdit.height);

        // context.drawImage(img,                     x,                                         y);
        ctxBuffer.drawImage(this.boxEdit, this.posU*buffer.width - (this.boxEdit.width/2), this.posV*buffer.height - (this.boxEdit.height/2));
    }

    /**
     * Draws the loaded image on the box.
     * @returns {void}
     */
    drawImg() {
        // Rotate canvas to an specific angle
        let x = this.boxEdit.width / 2
        let y = this.boxEdit.height / 2;

        // Rotate Canvas
        this.ctxBoxEdit.save();
        this.ctxBoxEdit.translate(x, y);
        this.ctxBoxEdit.rotate(this.handlers[3].getRotationAngleInRads());

        // Setting text config
        // this.ctxBoxEdit.font = "Bold " + this.textSize + "px " + this.textFont;
        // this.ctxBoxEdit.fillStyle = this.textColor;
        // this.ctxBoxEdit.textBaseline = "middle";
        // this.ctxBoxEdit.textAlign = "center";

        // Set the text to the canvas context
        this.ctxBoxEdit.translate(-this.getWidthBox()/2, -this.getHeightBox()/2);
        this.ctxBoxEdit.drawImage(this.img, 0, 0, this.getWidthBox(), this.getHeightBox());

        // FOR TESTING
        // this.ctxBoxEdit.fillStyle = "#000";
        // this.ctxBoxEdit.fillRect(-(this.widthBox/2), -(this.heightBox/2), this.widthBox, this.heightBox);
        // this.ctxBoxEdit.fillStyle = "#F0F";
        // this.ctxBoxEdit.fillText(this.textContent, 0, (this.heightBox*0.1));
        // this.ctxBoxEdit.fillStyle = "#FFF";
        // this.ctxBoxEdit.fillRect(-(this.iconSize/2), -(this.iconSize/2), this.iconSize, this.iconSize);

        this.ctxBoxEdit.restore();
    }

    /**
     * Draws the editing lines and corners around the embroidery.
     * @param {number} squaredCornersPercentage - The relative size's percetange in regards to the total box's size
     * @param {boolean} draw - Boolean that tells me if I have to draw the image embroidery or no
     * @param {String} color - The line and little square's color.
     */
    drawLinesAndCorners(squaredCornersPercentage, draw=true, color='white') {
        let sizeRect = this.textSize*squaredCornersPercentage;
        let angleRads = this.handlers[3].getRotationAngleInRads();
        let container = getBoxSizes(this.widthBox, this.heightBox, angleRads);

        // get bounding box
        let bx1 = 0 + this.iconSize;
        let by1 = 0 + this.iconSize;
        let bx2 = container.w + this.iconSize;
        let by2 = container.h + this.iconSize;

        this.boxEdit.width = container.w + (this.iconSize*2);
        this.boxEdit.height = container.h + (this.iconSize*2);

        if(draw) {
            this.ctxBoxEdit.strokeStyle = color;
            this.ctxBoxEdit.strokeRect(this.iconSize, this.iconSize, container.w, container.h);

            // Squared corners
            this.ctxBoxEdit.fillStyle = color;
            // Esquina cuadrada superior izquierda
            this.ctxBoxEdit.fillRect(bx1, by1, sizeRect, sizeRect);
            // Esquina cuadrada inferior izquierda
            this.ctxBoxEdit.fillRect(bx1, by2 - sizeRect, sizeRect, sizeRect);
            // Esquina cuadrada superior derecha
            this.ctxBoxEdit.fillRect(bx2 - sizeRect, by1, sizeRect, sizeRect);
            // Esquina cuadrada inferior derecha
            this.ctxBoxEdit.fillRect(bx2 - sizeRect, by2 - sizeRect, sizeRect, sizeRect);
        }
    }

    /**
     * Set a new value for the embroidery's image.
     * @param {Image} img - New image for the embroidery.
     */
    setImage(img) {
        this.img = img;
    }
}