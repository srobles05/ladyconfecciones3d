/**
 * @fileoverview A class oriented to describe each object embroidery on each model's section.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 --
 * 
*/

import { DeleteHandler } from "./handlers/DeleteHandler.js";
import { FixedHandler } from "./handlers/FixedHandler.js";
import { ZoomHandler } from "./handlers/ZoomHandler.js";
import { RotateHandler } from "./handlers/RotateHandler.js";
import { EditHandler } from "./handlers/EditHandler.js";

/**
 * A general class for all the embroideries that will be used on the model.
 * @class
 */
export class Embroidery {

    /**
     * @constructor
     * @param {string} section - String with the model's section name.
     * @param {string} typeEmbroidery - String with the stamp's class name.
     * @param {string} modelName - String with the model's name.
     * @param {number} posU - Position taken from the 3D to the 2D projection.
     * @param {number} posV - Position taken from the 3D to the 2D projection.
     * @param {image} image - Generated image.
     */
    constructor(section, typeEmbroidery, modelName, handlerImages, posU=0, posV=0) {

        this.modelName = modelName;
        this.section = section;
        this.typeEmbroidery = typeEmbroidery;
        this.posU = posU;
        this.posV = posV;
        //--------------------------
        this.widthBox  = null;
        this.heightBox = null;

        this.visible = true;
        this.editing = false;
        this.iconSize = 25;

        // Handlers -> Se le puede pasar la localización al handler por parametro
        // ej: new FixedHandler('down_left') si no se le pasa queda por defecto
        // Delete -> down_left
        // Fixed -> down_right
        // Zoom -> up_left
        // Rotate -> up_right
        
        this.handlers = [
            new DeleteHandler(handlerImages[0], 'down_left'), 
            new FixedHandler(handlerImages[1], 'up_left'), 
            new ZoomHandler(handlerImages[2], 'up_right'),
            new RotateHandler(handlerImages[3], 'down_right')
        ];
        
        if(handlerImages.length > 4)
            this.handlers.push(new EditHandler(handlerImages[4], 'down_middle'));
    }

    /**
     * Returns all the created handlers's on the embroidery.
     * @returns {Array}
     */
    getHandlers() {
        return this.handlers;
    }

    /**
     * Set the new section where the stamp will be located on the model.
     * @param {string} newSection - Represents the new section where the embroidery will be located.
     * @throws {TypeError} - Unexpected object type if it's not a string or doesn't belong to the available part's names.
     * @returns {void}
     */
    setSection(newSection) {
        if (typeof newSection === 'string' && db['models'][this.modelName]['parts_names'].includes(newSection)){
            this.section = newSection;
        } else {
            throw new TypeError("We got an unexpected object type. Please check if your variable is a string.");
        }
    }

    /**
     * Get the section's name where the embroidery is located.
     * @returns {string} - String with the model's section name where the embroidery is located.
     */
    getSection() {
        return this.section;
    }

    /**
     * Get the type Embroidery class name
     * @returns {string} - String with the Embroidery's class name.
     */
    getTypeEmbroidery() {
        return this.typeEmbroidery;
    }

    /**
     * Set the new model's name.
     * @param {string} newModelName - Should contain the new model's name where the embroidery will be located.
     * @throws {TypeError} - Returns an error when the new model's name is not a string or is not included in the local dabase file located at /js/db.js
     * @returns {void}
     */
    setModelName(newModelName) {
        if (typeof newModelName === 'string' && db['models'][newModelName]){
            this.section = newModelName;
        } else {
            throw new TypeError("We got an unexpected object type. Please check if your variable is a string.");
        }
    }

    /**
     * Set the new value for posU.
     * @param {number} newPosU - A parameter that should represent the new U position of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new U position is not a number.
     * @returns {void}
     */
    setPosU(newPosU) {
        if (typeof newPosU === 'number') this.posU = newPosU;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Get the U position of the stamp above the object.
     * @returns {number}
     */
    getPosU() {
        return this.posU;
    }

    /**
     * Set the new value for posV
     * @param {number} newPosV - A parameter that should represent the new V position of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new V position is not a number.
     * @returns {void}
     */
    setPosV(newPosV) {
        if (typeof newPosV === 'number') this.posV = newPosV;
        else throw new TypeError("We got an unexpected object type. Please check if you variable is a number.");
    }

    /**
     * Get the V position of the Embroidery above the object.
     * @returns {number}
     */
    getPosV() {
        return this.posV;
    }

    /**
     * Set the new value for posU and posV
     * @param {number} newPosU - A parameter that should represent the new U position of the embroidery above the object.
     * @param {number} newPosV - A parameter that should represent the new V position of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new U or V position is not a number.
     * @returns {void}
     */
    setPos(newPosU, newPosV) {
        this.setPosU(newPosU);
        this.setPosV(newPosV);
    }

    /**
     * Set a new box's width value.
     * @param {number} newWidth - New box's/embroidery's width.
     */
    setWidthBox( newWidth ){
        if (typeof newWidth === 'number') this.widthBox = newWidth;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Set a new box's height value.
     * @param {number} newHeight - New box's/embroidery's height.
     */
    setHeightBox( newHeight ){
        if (typeof newHeight === 'number') this.heightBox = newHeight;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }
    
    /**
     * Returns the actual box's width value.
     * @returns {number}
     */
    getWidthBox( ){
        return this.widthBox;
    }

    /**
     * Returns the actual box's height value.
     * @returns {number}
     */
    getHeightBox( ){
        return this.heightBox;                
    }

    /**
     * Set a new embroidery's visible value.
     * @param {boolean} visible - Boolean that lets the embroidery be seen or not.
     */
    setVisible(visible) {
        if (typeof visible === 'boolean') this.visible = visible;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }

    /**
     * Returns the actual embroidery's visible value.
     * @returns {boolean}
     */
    getVisible() {
        return this.visible;
    }

    /**
     * Set the text content.
     * @param {string} newEditing - Should contain the new editing value for the embroidery's edition.
     * @throws {TypeError} - Returns an error when the new editing value is not a boolean.
     * @returns {void}
     */
    setEditing(newEditing) {
        if (typeof newEditing === 'boolean'){
            this.editing = newEditing;
        } else {
            throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
        }
    }

    /**
     * Get editing value for this stamp
     * @returns {boolean}
     */
    getEditing() {
        return this.editing;
    }
}