/**
 * @fileoverview A class oriented to describe a text embroidery on each model's section.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 *
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 --
 * ----
 *
*/

// Importing Embroidery
import { Embroidery } from "./Embroidery.js";

import {
    getCorner,
    getBoxSizes
} from "../others/outils.js";

/**
 * A class for all embroideries that will have text on the model.
 * @class
 */
export class TextEmbroidery extends Embroidery {

    /**
     * @constructor
     * @extends {embroideries.Embroidery} - Extends from the class Embroidery located at /js/embroideries/Embroidery.js
     * @param {string} modelName - String with the model's name.
     * @param {string} section - String with the model's section name.
     * @param {number} textSize - Represents the text's size that will be used to create the embroidery.
     * @param {string} textFont - Represents the text font [TODO](Should be analized with the customer).
     * @param {string} textContent - Includes the text that will be shown on the model.
     * @param {number} posU - Position taken from the 3D to the 2D projection.
     * @param {number} posV - Position taken from the 3D to the 2D projection.
     */
    constructor(modelName, section, textSize, textFont, textContent, textColor, handlerImages, posU=0, posV=0) {
        super(section, "TextEmbroidery", modelName, handlerImages, posU, posV);

        this.textSize = textSize;
        this.textFont = textFont;
        this.textContent = textContent;
        this.textColor = textColor;

        this.handlers[2].setScalingFactor(this.textSize);

        // this.iconSize = this.textSize - (this.textSize*0.6);

        // Getting canvas and working with it
        this.boxEdit = document.createElement('canvas');

        // Getting context
        this.ctxBoxEdit = this.boxEdit.getContext('2d');

        this.textCanvas = document.createElement('canvas');
        this.ctxTextCanvas = this.textCanvas.getContext('2d');
    }

    /**
     * 
     * @param {Object} data 
     * @returns {void}
     */
    update(data) {
        if(data['visible'] != undefined) this.setVisible(data['visible']);
        if(data['type'] != undefined) this.setTextFont(data['type']);
        if(data['value'] != undefined) this.setTextContent(data['value']);
        if(data['color'] != undefined) this.setColor(data['color']);
    }

    /**
     * Draw text in a canvas buffer.
     * @param {canvas} buffer - Canvas buffer.
     */
    draw(buffer) {
        if(!this.getVisible())
            return;

        let ctxBuffer = buffer.getContext('2d');
        this.measureTextHeight();

        // If this embroiderys is selected for editing
        this.drawLinesAndCorners(0.08, this.editing);
        if(this.editing) {
            // Drawing handlers
            // context.drawImage(     img,             sx,sy, swidth,sheight,x,  y,                   width,height);

            // When fixedEmbroidery is activated we'll just draw it, otherwise we'll draw all the handlers
            if (this.handlers[1].getFixed()) {

                this.handlers.forEach((handler, idx) => {
                    // Fixed embroidery position
                    if (idx == 1)
                        this.handlers[1].draw(this.ctxBoxEdit, this.boxEdit.width, this.boxEdit.height, this.iconSize);
                    else
                        handler.setDrawn(false);
                });

            } else {
                this.handlers.forEach(handler => {
                    handler.draw(this.ctxBoxEdit, this.boxEdit.width, this.boxEdit.height, this.iconSize);
                });
            }
        }

        this.drawText();
        
        // this.setWidthBox(this.boxEdit.width);
        // this.setHeightBox(this.boxEdit.height);

        // context.drawImage(img,                     x,                                         y);
        ctxBuffer.drawImage(this.boxEdit, this.posU*buffer.width - (this.boxEdit.width/2), this.posV*buffer.height - (this.boxEdit.height/2));
        
        // ctxBuffer.beginPath();
        // ctxBuffer.arc(this.posU*buffer.width, this.posV*buffer.width, 5, 0, 2*Math.PI);
        // ctxBuffer.arc(this.posU*buffer.width - (this.boxEdit.width/2), this.posV*buffer.width - (this.boxEdit.height/2), 5, 0, 2*Math.PI);
        // ctxBuffer.arc(this.posU*buffer.width - this.boxEdit.width/2, this.posV*buffer.width + (this.boxEdit.height/2), 5, 0, 2*Math.PI);
        // ctxBuffer.arc(this.posU*buffer.width + this.boxEdit.width/2, this.posV*buffer.width - (this.boxEdit.height/2), 5, 0, 2*Math.PI);
        // ctxBuffer.arc(this.posU*buffer.width + this.boxEdit.width/2, this.posV*buffer.width + (this.boxEdit.height/2), 5, 0, 2*Math.PI);
        // ctxBuffer.arc(this.posU*buffer.width, this.posV*buffer.width + (this.boxEdit.height/2), 5, 0, 2*Math.PI);
        // ctxBuffer.fillStyle = "red";
        // ctxBuffer.fill();
    }

    /**
     * Measures the box's width and height.
     * @returns {void}
     */
    measureTextHeight() {
        // If I am zooming in or out I need to update the text size
        if (this.handlers[2].getZoomState()) {
            this.textSize = this.handlers[2].getScalingFactor();
        }
        
        // Configuring text
        this.ctxTextCanvas.font = "Bold " + this.textSize + "px "+this.textFont;
        this.ctxTextCanvas.fillStyle = "rgba(1,1,1,1)";
        this.ctxBoxEdit.textBaseline = "middle";
        this.ctxBoxEdit.textAlign = "center";

        let metrics = this.ctxTextCanvas.measureText(this.textContent);

        // Initial height aproximation
        let h = this.ctxTextCanvas.measureText("M").width;

        // Setting width and height
        this.setWidthBox(metrics.width);
        this.setHeightBox(h);
        this.textCanvas.width = metrics.width + this.iconSize;
        this.textCanvas.height = h + this.iconSize;

        if (!this.editing) {
            // // console.log("METRICS: ", metrics, " H: ", h);
            this.boxEdit.width = this.widthBox + ( this.iconSize * 2 );
            this.boxEdit.height = this.heightBox + ( this.iconSize * 2 );
        }
        // this.textCanvas.width = this.boxEdit.width;
        // this.textCanvas.height = this.boxEdit.height;
    }

    /**
     * Draws the editing lines and corners around the embroidery.
     * @param {number} squaredCornersPercentage - The relative size's percetange in regards to the total box's size
     * @param {boolean} draw - Boolean that tells me if I have to draw the image embroidery or no
     * @param {String} color - The line and little square's color.
     */
    drawLinesAndCorners(squaredCornersPercentage, draw=true, color='white') {
        let sizeRect = this.textSize*squaredCornersPercentage;
        let angleRads = this.handlers[3].getRotationAngleInRads();
        let container = getBoxSizes(this.widthBox, this.heightBox, angleRads);

        // get bounding box
        let bx1 = 0 + this.iconSize;
        let by1 = 0 + this.iconSize;
        let bx2 = container.w + this.iconSize;
        let by2 = container.h + this.iconSize;

        this.boxEdit.width = container.w + (this.iconSize*2);
        this.boxEdit.height = container.h + (this.iconSize*2);

        if(draw) {
            this.ctxBoxEdit.strokeStyle = color;
            this.ctxBoxEdit.strokeRect(this.iconSize, this.iconSize, container.w, container.h);

            // Squared corners
            this.ctxBoxEdit.fillStyle = color;
            // Esquina cuadrada superior izquierda
            this.ctxBoxEdit.fillRect(bx1, by1, sizeRect, sizeRect);
            // Esquina cuadrada inferior izquierda
            this.ctxBoxEdit.fillRect(bx1, by2 - sizeRect, sizeRect, sizeRect);
            // Esquina cuadrada superior derecha
            this.ctxBoxEdit.fillRect(bx2 - sizeRect, by1, sizeRect, sizeRect);
            // Esquina cuadrada inferior derecha
            this.ctxBoxEdit.fillRect(bx2 - sizeRect, by2 - sizeRect, sizeRect, sizeRect);
        }
    }

    // // Old Implementation
    // drawLinesAndCorners(squaredCornersPercentage, color='white') {
    //     let sizeRect = this.textSize*squaredCornersPercentage;
    //     let angleRads = this.handlers[3].getRotationAngleInRads();

    //     let rect = [this.iconSize, this.iconSize, this.widthBox, this.heightBox];

    //     // get corner coordinates
    //     let c1 = getCorner(this.boxEdit.width/2, this.boxEdit.height/2, rect[0], rect[1], angleRads);
    //     let c2 = getCorner(this.boxEdit.width/2, this.boxEdit.height/2, rect[0] + rect[2], rect[1], angleRads);
    //     let c3 = getCorner(this.boxEdit.width/2, this.boxEdit.height/2, rect[0] + rect[2], rect[1] + rect[3], angleRads);
    //     let c4 = getCorner(this.boxEdit.width/2, this.boxEdit.height/2, rect[0], rect[1] + rect[3], angleRads);

    //     let container = getBoxSizes(this.widthBox, this.heightBox, angleRads);

    //     // get bounding box
    //     let bx1 = Math.min(c1.x, c2.x, c3.x, c4.x);
    //     let by1 = Math.min(c1.y, c2.y, c3.y, c4.y);
    //     let bx2 = Math.max(c1.x, c2.x, c3.x, c4.x);
    //     let by2 = Math.max(c1.y, c2.y, c3.y, c4.y);

    //     // this.setWidthBox(bx2 - bx1);
    //     // this.setHeightBox(by2 - by1);

    //     // // console.log("METRICS: ", metrics, " H: ", h);
    //     // this.boxEdit.width = this.widthBox + ( this.iconSize * 2 );
    //     // this.boxEdit.height = this.heightBox + ( this.iconSize * 2 );
    //     this.boxEdit.width = (bx2 - bx1) + (this.iconSize*2);
    //     this.boxEdit.height = (by2 - by1) + (this.iconSize*2);

    //     // let bounds = [bx1, by1, bx2 - bx1, by2 - bx1];

    //     this.ctxBoxEdit.strokeStyle = color;
    //     // this.ctxBoxEdit.strokeRect(bx1, by1, bx2 - bx1, by2 - by1);

    //     // FOR TESTING
    //     this.ctxBoxEdit.strokeRect(this.iconSize, this.iconSize, container.w, container.h);

    //     // // Squared corners
    //     // this.ctxBoxEdit.fillStyle = color;
    //     // // Esquina cuadrada superior izquierda
    //     // this.ctxBoxEdit.fillRect(bx1, by1, sizeRect, sizeRect);
    //     // // Esquina cuadrada inferior izquierda
    //     // this.ctxBoxEdit.fillRect(bx1, by2 - sizeRect, sizeRect, sizeRect);
    //     // // Esquina cuadrada superior derecha
    //     // this.ctxBoxEdit.fillRect(bx2 - sizeRect, by1, sizeRect, sizeRect);
    //     // // Esquina cuadrada inferior derecha
    //     // this.ctxBoxEdit.fillRect(bx2 - sizeRect, by2 - sizeRect, sizeRect, sizeRect);

    //     // Updating height and width
    //     // this.boxEdit.width = (bx2 - bx1) + (this.iconSize*2);
    //     // this.boxEdit.height = (by2 - by1) + (this.iconSize*2);
    // }

    /**
     * Draws the loaded text on the box.
     * @returns {void}
     */
    drawText() {
        // Rotate canvas to an specific angle
        let x = this.boxEdit.width / 2
        let y = this.boxEdit.height / 2;

        // Rotate Canvas
        this.ctxBoxEdit.save();
        this.ctxBoxEdit.translate(x, y);
        this.ctxBoxEdit.rotate(this.handlers[3].getRotationAngleInRads());

        // Setting text config
        this.ctxBoxEdit.font = "Bold " + this.textSize + "px " + this.textFont;
        this.ctxBoxEdit.fillStyle = this.textColor;
        this.ctxBoxEdit.textBaseline = "middle";
        this.ctxBoxEdit.textAlign = "center";

        // Set the text to the canvas context
        this.ctxBoxEdit.fillText(this.textContent, 0, (this.heightBox*0.1));

        // FOR TESTING
        // this.ctxBoxEdit.fillStyle = "#000";
        // this.ctxBoxEdit.fillRect(-(this.widthBox/2), -(this.heightBox/2), this.widthBox, this.heightBox);
        // this.ctxBoxEdit.fillStyle = "#F0F";
        // this.ctxBoxEdit.fillText(this.textContent, 0, (this.heightBox*0.1));
        // this.ctxBoxEdit.fillStyle = "#FFF";
        // this.ctxBoxEdit.fillRect(-(this.iconSize/2), -(this.iconSize/2), this.iconSize, this.iconSize);

        this.ctxBoxEdit.restore();
    }

    // // Old Implementation
    // drawText() {
    //     // clear canvas
    //     this.ctxTextCanvas.clearRect(0, 0, this.textCanvas.width, this.textCanvas.height);

    //     // If I am zooming in or out I need to update the text size
    //     if (this.handlers[2].getZoomState()) {
    //         this.textSize = this.handlers[2].getScalingFactor();
    //     }

    //     // this.iconSize = this.textSize - (this.textSize*0.6);

    //     // Rotate canvas to an specific angle
    //     this.rotateCanvasText(this.boxEdit.width / 2, this.boxEdit.height / 2);

    //     // Setting text config
    //     this.ctxTextCanvas.font = "Bold " + this.textSize + "px " + this.textFont;
    //     this.ctxTextCanvas.fillStyle = this.textColor;
    //     this.ctxTextCanvas.textBaseline = "top";

    //     // Set the text to the canvas context
    //     // this.ctxTextCanvas.fillText(this.textContent, this.iconSize, this.iconSize);

    //     this.ctxTextCanvas.restore();

    //     this.ctxBoxEdit.drawImage(this.textCanvas, 0, 0);
    // }

    /**
     * Rotates text's canvas to a specific coordinate.
     * @param {number} x - X axis position where the text's canvas will rotate.
     * @param {number} y - Y axis position where the text's canvas will rotate.
     */
    rotateCanvasText(x, y) {
        this.ctxTextCanvas.save();
        this.ctxTextCanvas.translate(x, y);
        this.ctxTextCanvas.rotate(this.handlers[3].getRotationAngleInRads());
        this.ctxTextCanvas.translate(-x, -y);
    }

    /**
     * Set the new value for TextSize.
     * @param {number} newTextSize - Represents the text's size that will be used to create the embroidery.
     * @throws {TypeError} - Returns an error when the new TextSize variable is not a number.
     * @returns {void}
     */
    setTextSize(newTextSize) {
        if (typeof newTextSize === 'number') {
            this.textSize = newTextSize;
        }
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Get the actual text size of the embroidery above the object.
     * @returns {number}
     */
    getTextSize() {
        return this.textSize;
    }

    /**
     * Set the new text font.
     * @param {string} newTextFont - Should contain the new text font name for the embroidery.
     * @throws {TypeError} - Returns an error when the new text font is not a string or is not included in the local dabase file located at /js/db.js
     * @returns {void}
     */
    setTextFont(newTextFont) {
        if (typeof newTextFont === 'string') {
            this.textFont = newTextFont;
        } else {
            throw new TypeError("We got an unexpected object type. Please check if your variable is a string.");
        }
    }

    /**
     * Get the embroidery's actual text font.
     * @returns {string} - String with the fontname
     */
    getTextFont() {
        return this.textFont;
    }

    /**
     * Set the text content.
     * @param {string} newTextContent - Should contain the new content for the embroidery.
     * @throws {TypeError} - Returns an error when the new text font is not a string or is not included in the local dabase file located at /js/db.js
     * @returns {void}
     */
    setTextContent(newTextContent) {
        if (typeof newTextContent === 'string'){
            this.textContent = newTextContent;
        } else {
            throw new TypeError("We got an unexpected object type. Please check if your variable is a string.");
        }
    }

    /**
     * Get the embroidery's actual text content.
     * @returns {string} - String with the text content.
     */
    getTextContent() {
        return this.textContent;
    }

    /**
     * Set the text color.
     * @param {string} newColor.
     * @throws {TypeError} - Returns an error when the new editing value is not a boolean.
     * @returns {void}
     */
    setColor(newColor) {
        if (typeof newColor === 'string'){
            this.textColor = newColor;
        } else {
            throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
        }
    }

    /**
     * Get color value for this embroidery
     * @returns {boolean}
     */
    getColor() {
        return this.textColor;
    }
}