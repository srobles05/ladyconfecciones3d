/**
 * @fileoverview A class oriented to describe a handler on each embroidery.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 -- 
 * ----
 * 
*/

/**
 * A class for all handlers that will have embroideries on the model.
 * @class
 */
export class Handler {

    /**
     * @constructor
     * @param {String} handlerType - It has the handler's type name. 
     * @param {Image} image - It has the selected image for that handler. 
     * @param {String} location - It has the selected location (up_left, up_right, down_left, down_middle, down_right). 
     * @param {number} posU - Position taken from the 3D to the 2D projection. 
     * @param {number} posV - Position taken from the 3D to the 2D projection. 
     */
    constructor(handlerType, image, location, posU, posV) {
        this.handlerType = handlerType;
        this.posU = posU;
        this.posV = posV;
        this.handlerImage = image;
        this.location = location;
        this.iconSize = 25;
        this.alternativeIcon = false;
        this.isDrawn = false;
    }
    
    /**
     * Draws the icon on the icon's location.
     * @param {CanvasContext} embroideryCanvasContext - Embroidery's canvas context.
     * @param {number} embroideryWidth - Embroidery's box's width.
     * @param {number} embroideryHeight - Embroidery's box's height.
     * @param {number} iconSize - Icon size.
     * @returns {void}
     */
    draw(embroideryCanvasContext, embroideryWidth, embroideryHeight, iconSize) {
        // context.drawImage(         img,             sx,sy, swidth,sheight,x,  y,                   width,height);
        
        this.setIconSize(iconSize);
        let x = 0;
        let y = 0; 

        if(this.location == 'down_left') {
            // Esquina izquierda inferior
            x = 0;
            y = embroideryHeight-iconSize;
        }
        else if(this.location == 'down_right') {
            // Esquina derecha inferior
            x = embroideryWidth-iconSize;
            y = embroideryHeight-iconSize;
        }
        else if(this.location == 'down_middle') {
            x = (embroideryWidth/2) - (this.iconSize/2);
            y = embroideryHeight-iconSize;
        }
        else if(this.location == 'up_left') {
            // Esquina izquierda superior
            x = 0;
            y = 0;
        }
        else if(this.location == 'up_right') {
            // Esquina derecha superior
            x = embroideryWidth - iconSize;
            y = 0;
        }
        // Dejo preparado para algún else u otra ubicación

        let startImagePosition = 0;

        if(this.alternativeIcon)
            startImagePosition = 64;
        
        this.setDrawn(true);
        embroideryCanvasContext.drawImage(this.handlerImage, startImagePosition, 0, 64, 64, x, y, this.iconSize, this.iconSize);
    }

    /**
     * Checks if the handler is clicked.
     * @param {number} x - Coordinate x on the 3D model clicked with the mouse
     * @param {number} y - Coordinate y on the 3D model clicked with the mouse
     * @param {number} embroideryPosU - Embroidery's pos U
     * @param {number} embroideryPosV - Embroidey's pos V
     * @param {Canvas} back - Section's canvas 
     * @param {number} embroideryWidth - Embroidery's width
     * @param {number} embroideryHeight - Embroidery's heigth 
     * @returns {void}
     */
    isClickIn(x, y, embroideryPosU, embroideryPosV, back, embroideryWidth, embroideryHeight) {
        embroideryWidth += (this.iconSize*2);
        let localWidth = this.iconSize;
        let localHeight = this.iconSize;
        let localTx = (embroideryPosU * back.width);
        let localTy = (embroideryPosV * back.height);

        if (this.isDrawn) {

            if (this.location == 'down_left') {
                // Me ubico en la esquina inferior izquierda (referente a la bordado)
                localTx += - (embroideryWidth / 2);
                localTy += (embroideryHeight / 2);

                if ( (x >= localTx && x <= localTx + localWidth) && (y >= localTy && y <= localTy + localHeight) ) {
                    return true;
                }
            }
            else if(this.location == 'down_right') {
                // Me ubico en la esquina inferior derecha
                localTx += (embroideryWidth / 2);
                localTy += (embroideryHeight / 2);
    
                if ( (x >= localTx - localWidth && x <= localTx) && (y >= localTy && y <= localTy + localHeight) ) {
                    return true;
                }
            }
            else if(this.location == 'up_left') {
                // Me ubico en la esquina superior izquierda
                localTx += - (embroideryWidth / 2);
                localTy += - (embroideryHeight / 2);
    
                if ( (x >= localTx && x <= localTx + localWidth) && (y >= localTy - localHeight && y <= localTy) ) {
                    return true;
                }
            }
            else if (this.location == 'up_right'){
                // Me ubico en la esquina superior derecha 
                localTx += (embroideryWidth / 2);
                localTy += - (embroideryHeight / 2);
                                
                if ( (x >= localTx - localWidth && x <= localTx ) && (y >= localTy - localHeight && y <= localTy) ) {
                    return true;
                }
            }
            else if (this.location == 'down_middle'){
                // Me ubico en el medio-inferior
                localTx += - (this.iconSize / 2);
                localTy += (embroideryHeight/2);
                
                if ( (x >= localTx && x <= localTx + localWidth) && (y >= localTy && y <= localTy + localHeight) ) {
                    return true;
                }
            }
            // Dejo preparado para algún else u otra ubicación
            // console.log(`Location: ${this.location} localWidth: ${localWidth} localHeight: ${localHeight} localTx: ${localTx} localTy: ${localTy} x: ${x} y: ${y}`)

        }

        return false;
    }

    /**
     * Set a new Drawn value.
     * @param {boolean} newDrawnValue - Boolean that determines if the handler should be seen or not
     * @returns {void}
     */
    setDrawn(newDrawnValue) {
        if (typeof newDrawnValue === 'boolean') this.isDrawn = newDrawnValue;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }

    /**
     * Returns drawn's value.
     * @returns {boolean}
     */
    getDrawn() {
        return this.isDrawn;
    }

    /**
     * Get the handler's location
     * @returns {string} - String with the handler's location
     */
    getHandlerType() {
        return this.handlerType;
    }

    /**
     * Set the new value for posU.
     * @param {number} newPosU - A parameter that should represent the new U position of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new U position is not a number.
     * @returns {void}
     */
    setPosU(newPosU) {
        if (typeof newPosU === 'number') this.posU = newPosU;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Get the U position of the embroidery above the object.
     * @returns {number}
     */
    getPosU() {
        return this.posU;
    }

    /**
     * Set the new value for posV
     * @param {number} newPosV - A parameter that should represent the new V position of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new V position is not a number.
     * @returns {void}
     */
    setPosV(newPosV) {
        if (typeof newPosV === 'number') this.posV = newPosV;
        else throw new TypeError("We got an unexpected object type. Please check if you variable is a number.");
    }

    /**
     * Get the V position of the embroidery above the object.
     * @returns {number}
     */
    getPosV() {
        return this.posV;
    }

    /**
     * @returns {number}
     */
    getIconSize(){
        return this.iconSize;
    }

    /**
     * 
     * @param {*} newIconSize 
     * @throws {TypeError} - Returns an error when the new icon size is not a number.
     * @returns {void}
     */
    setIconSize( newIconSize ){
        if (typeof newIconSize === 'number') this.iconSize = newIconSize;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Set the new value for posU and posV
     * @param {number} newPosU - A parameter that should represent the new U position of the embroidery above the object.
     * @param {number} newPosV - A parameter that should represent the new V position of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new U or V position is not a number.
     * @returns {void}
     */
    setPos(newPosU, newPosV) {
        this.setPosU(newPosU);
        this.setPosV(newPosV);
    }

    /**
     * Set the new value for alternativeIcon.
     * @param {boolean} newAlternativeIcon - A parameter that should represent the new alternative icon of the embroidery above the object.
     * @throws {TypeError} - Returns an error when the new alternativeIcon value is not a boolean.
     * @returns {void}
     */
    setAlternativeIcon(newAlternativeIcon) {
        if (typeof newAlternativeIcon === 'boolean') this.alternativeIcon = newAlternativeIcon;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }
}