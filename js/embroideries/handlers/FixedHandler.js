/**
 * @fileoverview A class oriented to describe a delete handler on each embroidery.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 -- 
 * ----
 * 
*/

import { Handler } from "./Handler.js";

/**
 * A class for fixed handler option that will have text embroideries on the model.
 * @class
 */
export class FixedHandler extends Handler {

    /**
     * @constructor
     * @param {Image} img - Fixed's icon image
     * @param {String} location - Location where the delete icon will be located.
     * @param {number} posU - Position taken from the 3D to the 2D projection. 
     * @param {number} posV - Position taken from the 3D to the 2D projection. 
     */
    constructor(img, location='down_right', posU=0, posV=0) {
        super('fixed', img, location, posU, posV);
        this.fixed = false;
    }

    /**
     * Sets the embroidery as fixed on the 3D model.
     * @returns {void}
     */
    fixedAction() {
        this.setFixed(!this.fixed);
        this.setAlternativeIcon(!this.alternativeIcon);
    }

    /**
     * Returns the actual fixed value.
     * @returns {boolean}
     */
    getFixed() {
        return this.fixed;
    }
    
    /**
     * Sets the new fixed value.
     * @param {boolean} newFixedValue 
     */
    setFixed(newFixedValue) {
        if (typeof newFixedValue === 'boolean') this.fixed = newFixedValue;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }
}