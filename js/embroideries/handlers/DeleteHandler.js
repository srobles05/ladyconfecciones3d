/**
 * @fileoverview A class oriented to describe a delete handler on each embroidery.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 -- 
 * ----
 * 
*/

import { Handler } from "./Handler.js";

/**
 * A class for delete handlers that will have text embroideries on the model.
 * @class
 */
export class DeleteHandler extends Handler {

    /**
     * @constructor
     * @param {Image} img - Delete's icon image
     * @param {String} location - Location where the delete icon will be located.
     * @param {number} posU - Position taken from the 3D to the 2D projection. 
     * @param {number} posV - Position taken from the 3D to the 2D projection. 
     */
    constructor(img, location='down_left', posU=0, posV=0) {
        super('delete', img, location, posU, posV);
    }

    /**
     * 
     */
    deleteStamp() {
        
    }
}