/**
 * @fileoverview A class oriented to describe a rotate handler on each embroidery.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 -- 
 * ----
 * 
*/

import { Handler } from "./Handler.js";

/**
 * A class for rotate handlers that will have text embroideries on the model.
 * @class
 */
export class RotateHandler extends Handler {

    /**
     * @constructor
     * @param {Image} img - Rotates's icon image
     * @param {String} location - Location where the delete icon will be located.
     * @param {number} posU - Position taken from the 3D to the 2D projection. 
     * @param {number} posV - Position taken from the 3D to the 2D projection. 
     */
    constructor(img, location='up_right', posU=0, posV=0) {
        super('rotate', img, location, posU, posV);
        this.rotationAngle = 0;
        this.oldRotationAngle = undefined;
        this.rotateState = false;
    }

    /**
     * Gets the rotateState value.
     * @returns {boolean}
     */
    getRotateState() {
        return this.rotateState;
    }

    /**
     * Sets a new rotateState value
     * @param {boolean} newRotateState - Boolean that determines if the embroidery is rotating or not.
     */
    setRotateState(newRotateState) {
        if (typeof newRotateState === 'boolean') this.rotateState = newRotateState;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }

    /**
     * Changes the actual rotateState value including that the alternative icon should be showed.
     * @returns {void}
     */
    changeRotateState() {
        this.setAlternativeIcon(!this.alternativeIcon);
        this.setRotateState(!this.rotateState);

        if (!this.firstTime) this.firstTime = true;

        if(!this.rotateState)
            this.oldRotationAngle = undefined;
    }

    /**
     * Changes the rotationAngle value that means that the embroidery will be rotated.
     * @param {number} factorValue - The value in which the rotationAngle should be changed.
     */
    rotate(factorValue) {
        this.rotationAngle += factorValue;
        if (this.rotationAngle >= 360) this.setRotationAngle(0);
    }
    
    /**
     * Changes the rotationAngle value that means that the embroidery will be rotated.
     * @param {number} newRotationAngle - The value in which the rotationAngle should be changed.
     */
    dynamicRotate(newRotationAngle) {
        if(this.oldRotationAngle == undefined) {
            this.oldRotationAngle = this.rotationAngle;
        }
        this.rotationAngle = this.oldRotationAngle + newRotationAngle;
        if (this.rotationAngle >= 360) this.setRotationAngle(this.rotationAngle-360);
        else if (this.rotationAngle < 0) this.setRotationAngle(360-this.rotationAngle);
    }

    /**
     * Gets the rotationAngle value
     * @returns {number}
     */
    getRotationAngle() {
        return this.rotationAngle;
    }

    /**
     * Gets the rotationAngle value in rads.
     * @returns {number}
     */
    getRotationAngleInRads() {
        return this.rotationAngle*Math.PI/180;
    }

    /**
     * Sets the new rotationAngle value.
     * @param {number} newRotationAngle - Rotation Angle value that determines how rotated the embroidery will be shown. 
     */
    setRotationAngle(newRotationAngle) {
        if (typeof newRotationAngle === 'number') this.rotationAngle = newRotationAngle;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }
}