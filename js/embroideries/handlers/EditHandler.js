/**
 * @fileoverview A class oriented to describe an edit handler on each embroidery.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 * 
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 -- 
 * ----
 * 
*/

import { Handler } from "./Handler.js";

/**
 * A class for edit handlers that will have text embroideries on the model.
 * @class
 */
export class EditHandler extends Handler {

    /**
     * @constructor
     * @param {Image} img - Edit's icon image
     * @param {String} location - Location where the delete icon will be located.
     * @param {number} posU - Position taken from the 3D to the 2D projection. 
     * @param {number} posV - Position taken from the 3D to the 2D projection. 
     */
    constructor(img, location='down_middle', posU=0, posV=0) {
        super('edit', img, location, posU, posV);
    }

    /**
     * 
     * @param {number} index 
     * @returns {void}
     */
    editAction(index) {
        // this.setAlternativeIcon(!this.alternativeIcon);
        $('.card[data-index="'+index+'"] .edit-row-button').trigger('click');
    }
}