/**
 * @fileoverview A class oriented to describe a zoom handler on each embroidery.
 *
 * @version 1.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonab@unimagdalena.edu.co>
 *
 * @copyright LeidyConfecciones
 *
 * History
 * v1.0 --
 * ----
 *
*/

import { Handler } from "./Handler.js";

/**
 * A class for zoom handlers that will have text embroideries on the model.
 * @class
 */
export class ZoomHandler extends Handler {

    /**
     * @constructor
     * @param {Image} img - Zoom's icon image
     * @param {String} location - Location where the delete icon will be located.
     * @param {number} posU - Position taken from the 3D to the 2D projection. 
     * @param {number} posV - Position taken from the 3D to the 2D projection. 
     */
    constructor(img, location='up_left', posU=0, posV=0) {
        super('zoom', img, location, posU, posV);
        this.minimumFactor = 20;
        this.scalingFactor = 50;
        this.zoomState = false;
        this.lastX = 0;
        this.lastY = 0;
        this.firstTime = true;
    }

    /**
     * Gets the zoomState value
     * @returns {boolean}
     */
    getZoomState() {
        return this.zoomState;
    }

    /**
     * 
     * @param {boolean} newZoomState - Boolean that determines if the zoom handler is activated or not.
     */
    setZoomState(newZoomState) {
        if (typeof newZoomState === 'boolean') this.zoomState = newZoomState;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }

    /**
     * Changes the zoom state including also the alternativeIcon that means that another icon will be shown.
     * @returns {void}
     */
    changeZoomState() {
        this.setAlternativeIcon(!this.alternativeIcon);
        this.setZoomState(!this.zoomState);

        if (!this.firstTime) this.firstTime = true;
    }

    /**
     * Sets the new scalingFactor's value.
     * @param {number} newScalingFactor - The zoom's scaling factor that determines the zoom's change.
     */
    setScalingFactor(newScalingFactor) {
        if (typeof newScalingFactor === 'number') {
            if (newScalingFactor < this.minimumFactor) this.scalingFactor = this.minimumFactor;
            else this.scalingFactor = newScalingFactor;
        }
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Changes the scalingFactor's value.
     * @param {number} newValueX - Value to control the zoom change related with the actual's mouse coordinate.
     * @param {number} newValueY - Value to control the zoom change related with the actual's mouse coordinate. 
     */
    changeScalingFactor(newValueX, newValueY) {

        if (this.firstTime) {
            this.setLastX(newValueX);
            this.setLastY(newValueY);
            this.firstTime = false;
        } else {
            let higher = (this.lastX > this.lastY) ? [this.lastX, 0] : [this.lastY, 1];
            let difference = 0;

            if (higher[1] == 0) {
                difference = newValueX - this.lastX;
            } else {
                difference = newValueY - this.lastY;
            }

            this.setScalingFactor(this.scalingFactor + difference);
            this.setLastX(newValueX);
            this.setLastY(newValueY);
        }
    }

    /**
     * Gets the scalingFactor's value
     * @returns {number}
     */
    getScalingFactor() {
        return this.scalingFactor;
    }

    /**
     * Sets the new lastX value.
     * @param {number} newLastX - Control variable for the zoom's action. 
     */
    setLastX(newLastX) {
        if (typeof newLastX === 'number') this.lastX = newLastX;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Gets the lastX value
     * @returns {number}
     */
    getLastX() {
        return this.lastX;
    }

    /**
     * Sets the new lastY value.
     * @param {number} newLastY - Control variable for the zoom's action. 
     */
    setLastY(newLastY) {
        if (typeof newLastY === 'number') this.lastY = newLastY;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a number.");
    }

    /**
     * Gets the lastY value
     * @returns {number}
     */
    getLastY() {
        return this.lastY;
    }

    /**
     * Sets the new firstTime value.
     * @param {boolean} newFirstTimeValue - Boolean that determines if it's the first time the zoom's action is activated on the embroidery
     */
    setFirstTime(newFirstTimeValue) {
        if (typeof newFirstTimeValue === 'boolean') this.firstTime = newFirstTimeValue;
        else throw new TypeError("We got an unexpected object type. Please check if your variable is a boolean.");
    }

    /**
     * Gets the firstTime value
     * @returns {boolean}
     */
    getFirstTime() {
        return this.firstTime;
    }
}