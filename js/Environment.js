 /**
 * @fileoverview 3D model clothes viewer for clothes customization
 *
 * @version 4.0
 *
 * @author German Sanchez <sanchez.gt@gmail.com>
 * @author Sergio Robles <srobles.andres@gmail.com>
 * @author Camilo Laiton <camilolaitonb@gmail.com>
 *
 * @copyright LadyConfecciones
 *
 * History
 * v1.0 -- Initial environment class with the constructed 3D world.
 * v2.0 -- Adding new objects related to embroideries on the 3D model.
 * v3.0 -- Improving memory usage
 * v4.0 -- Refactoring and multiple canvas
*/

import {
    initialRotation,
    resizeRendererToDisplaySize,
    notify,
    getPartEmbroideryAvailability
} from "./others/outils.js";

// Importing Threejs Classes
import * as THREE from "../vendor/threejs/three.module.js";
import { OrbitControls } from "../vendor/threejs/OrbitControls.js";
import { GLTFLoader } from '../vendor/threejs/GLTFLoader.js';

// Importing own classes
import { TextEmbroidery } from "./embroideries/TextEmbroidery.js";
import { ImageEmbroidery } from "./embroideries/ImageEmbroidery.js";

// ****************** IGNORA ESTA SECCIÓN ********************************

// Function - Opening rotate
let initRotate = 0;

// *********************************************************************************

/**
 * A class for setting up the 3D environment in the navigator.
 * @class
 */
export class Environment {

    /**
     * @constructor
     * @param {string} modelPath - Filepath where the model's files are located.
     * @return {void}
     */
    constructor(modelPath, activeModelName) {
        // Threejs declarations
        this.modelPath = modelPath;
        this.model = null;
        this.scene = new THREE.Scene();

        // Loading Manager
        this.manager = new THREE.LoadingManager();

        this.manager.onProgress = function (item, loaded, total) {
            console.log(item, loaded, total);
        };

        let _this = this;

        this.manager.onLoad = function () {
            console.log('all items loaded');
            _this.initBuffers();
        };

        this.manager.onError = function () {
            console.log('there has been an error');
        };

        this.activeModelName = activeModelName

        // Loaders
        this.loader = new GLTFLoader(this.manager);
        this.textureLoader = new THREE.TextureLoader();

        // Threejs variables
        this.renderer = null;
        this.camera = null;
        this.controls = null;
        this.raycaster = null;
        this.dirLight = null;

        // WaterMark
        this.sprite = null;
        this.direction = new THREE.Vector3();

        // Embroideries
        this.isPlaceholderStamp = false;
        this.placeholderStamp = {};
        this.placeholderStampCallback = null;

        this.embroideries = [];
        this.selectedEmbroidery = -1;

        // canvas buffers
        this.buffers = []

        // Logic variables
        this.loaded = false;
        this.editing = false;
        this.handlerActivated = false;
        this.mouse = { x: 0, y: 0 };
        this.prevMouse = { x: undefined, y: undefined };
        this.intersectIndex = 1;

        // x and y for latest mouse position / x2 and y2 for i-1 th mouse position
        this.mouseText = { x: 0, y: 0, x2: 0, y2: 0};

        // General Canvas
        this.canvas = null;
        this.waterMark = null;
        this.firstDrawn = false;

        // Constante para mejorar el movimiento de la estampa
        this.mouseMoveCont = 0;

        this.handlerImages = [];
        this.initHandlerImages([
            'img/handlers/deleteHandler.png',
            'img/handlers/fixedHandler.png',
            'img/handlers/zoomHandler.png',
            'img/handlers/rotateHandler.png',
            'img/handlers/editHandler.png',
        ]);
    }

    initHandlerImages(pathImagesArray) {
        let handlerImage = null;

        pathImagesArray.forEach(pathImage => {
            handlerImage = new Image();
            handlerImage.onload = function() {
                console.log(`Handler ${pathImage} loaded!`);
            }
            handlerImage.src = pathImage;
            this.handlerImages.push(handlerImage);
        });
    }

    // Esta función se debe migrar a otra clase, eg. clase modelo
    getBufferIndex(section) {
        for (let index = 0; index < this.buffers.length; index++) {
            if (this.buffers[index].section == section) {
                return index;
            }
            // else if(
            //     (this.buffers[index].section == "manga_izquierda" || this.buffers[index].section == "manga_izquierda")
            //     && section == "mangas"
            // ) {
            //     // Si, es el if más feo de la historia de la humanidad pero no vi otra forma de hacerlo más bonito
            //     // porque desde el data-option viene como 'mangas'. (Visto por Sergio (JAJAJAJA))
            //     return index;
            // }
        }
        return null;
    }

    reset() {
        this.isPlaceholderStamp = false;
        this.placeholderStamp = {};
        this.placeholderStampCallback = null;
        this.embroideries = [];
        this.selectedEmbroidery = -1;

        this.buffers = [];
        this.initBuffers();
        this.setActiveSectionInDOM('mangas');
        this.selectSwatch({'target': {'dataset': {'key': '0'}}});
        this.setActiveSectionInDOM('base_mangas');
        this.selectSwatch({'target': {'dataset': {'key': '0'}}});
        this.setActiveSectionInDOM('frente');
        this.updateBuildTexturesAndColors();

        // Resetear el modelo. TO DO
    }

    blending(drawingSection) {
        let blend             = document.createElement('canvas');
        let ctxblend          = blend.getContext('2d');

        blend.width             = drawingSection.canvas.width;
        blend.height            = drawingSection.canvas.height;

        let colortela         = document.createElement('canvas');
        let ctxcolortela      = colortela.getContext('2d');

        colortela.width         = blend.width
        colortela.height        = blend.height

        ctxcolortela.fillStyle  = drawingSection.color;
        ctxcolortela.fillRect(0, 0, colortela.width, colortela.height);

        ctxblend.globalCompositeOperation = 'multiply';
        ctxblend.drawImage(drawingSection.back, 0, 0);
        ctxblend.drawImage(colortela, 0, 0);
        drawingSection.canvas.getContext('2d').drawImage(blend, 0, 0);
    }

    initBuffers() {
        let _this = this;

        this.model.children.forEach((section, idx) => {
            let drawingSection = {
                index: idx,
                section: section.name,
                back: new Image(),
                // color: "#a82d54",
                color: "#fff",
                canvas: document.createElement('canvas'),
                textureIndex: 0
            }

            drawingSection.back.onload = function() {

                drawingSection.canvas.width = drawingSection.back.width;
                drawingSection.canvas.height = drawingSection.back.height;

                if (section.name != "talla" && section.name != "etiqueta")
                    _this.updateEmbroideries(section.name);
            };

            // drawingSection.back.src = 'img/telablanca.jpg';
            // const item = db['clothes'][section.name];
            const item = db['models'][this.activeModelName]['clothes'][section.name];

            if(item != undefined)
                // drawingSection.back.src = db['clothes'][section.name][0]['texture'];
                drawingSection.back.src = db['models'][this.activeModelName]['clothes'][section.name][0]['texture'];

            _this.buffers.push(drawingSection);
        });
    }

    /**
     * Starts the 3D model viewer environment.
     * @param {string} domElementId - DOM element id where the 3D environment will appear.
     * @returns {void}
     */
    init(domElementId) {
        let _this = this;

        // Set background color
        const BACKGROUND_COLOR =  0xffffff;
        this.scene.background = new THREE.Color(BACKGROUND_COLOR);
        this.scene.fog = new THREE.Fog(BACKGROUND_COLOR, 20, 100);
        this.scene.fog.name = "fog";

        // Get canvas from DOM
        this.canvas = document.querySelector(domElementId);

        // Init the renderer
        this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas, antialias: true });
        this.renderer.shadowMap.enabled = true;
        // For incrementing realism
        this.renderer.shadowMapType = THREE.PCFSoftShadowMap;
        this.renderer.setPixelRatio(window.devicePixelRatio);
        this.renderer.outputEncoding = THREE.LinearEncoding;
        
        // Add a camera
        var cameraFar = 1;
        this.camera = new THREE.PerspectiveCamera(50, $('#canvasContainer').width() / $('#canvasContainer').height(), 0.01, 10000);
        this.camera.position.set(0, 0, cameraFar);
        this.camera.name = "camera";

        var map = new THREE.TextureLoader().load("img/logoLady.png");
        var material = new THREE.SpriteMaterial({
            map: map,
            color: 0xffffff,
            opacity: 0.1
        });
        this.sprite = new THREE.Sprite(material);
        this.sprite.name = "watermark"
        this.sprite.position.set(0,0,0);
        this.scene.add(this.sprite);

        // Raycaster
        this.raycaster = new THREE.Raycaster();

        // Measures of container canvas
        this.adjustContainerCanvasMeasures();

        // Load model
        this.loadModel3D(this.modelPath, cameraFar);

        // Add controls
        this.addControls();

        // Listeners
        this.addListeners();

        // Textures Carrousel
        this.buildPartsCarrousel();
        this.updateBuildTexturesAndColors();
        // this.buildTextures('#texturesBox', db['clothes'][this.getActiveSectionInDOM()], 'tray-parent-swatch');
        // this.buildTextures('#colorsBox', db['clothes'][this.getActiveSectionInDOM()][0]['colors'], 'tray-swatch', 0);

        // Draw
        this.draw();

        // Listener resize window
        $(window).resize(function() {
            _this.adjustContainerCanvasMeasures();
        });
    }

    /**
     * Main loop for canvas.
     * @returns {void}
     */
    draw() {
        requestAnimationFrame(this.draw.bind(this));
        this.render();
    }

    /**
     * Update all embroideries of the current section
     * @returns {void}
     */
    updateEmbroideries(section) {
        let activeSection;

        if(section != undefined && section != activeSection) {
            activeSection = section;
            // this.setActiveSectionInDOM(activeSection);
        } else {
            activeSection = this.getActiveSectionInDOM();
        }

        let index = this.getBufferIndex(activeSection);

        if(index != null) {
            this.blending(this.buffers[index]);
            // ctxBuffer.drawImage(this.buffers[index].back, 0, 0);
            if(this.embroideries.length > 0) {
                for(let stamp of this.embroideries) {
                    if(activeSection == stamp.getSection()) {
                        stamp.draw(this.buffers[index].canvas);
                    }
                }
            }

            let texture = new THREE.CanvasTexture(
                this.buffers[index].canvas,
                // THREE.UVMapping,
                // THREE.ClampToEdgeWrapping,
                // THREE.ClampToEdgeWrapping,
                // THREE.LinearFilter,
                // THREE.LinearMipmapLinearFilter,
                // THREE.LuminanceAlphaFormat,
                // THREE.UnsignedByteType,
                // this.renderer.getMaxAnisotropy(),
            );
            texture.flipY = false;

            // if(activeSection == "mangas") {
            //     // Sección para mangas
            //     this.setMaterial("manga_derecha", texture);
            //     this.setMaterial("manga_izquierda", texture);
            // } else {
                // Resto de secciones
            this.setMaterial(activeSection, texture);
            // }
        }
    }

    /**
     * Update active section in the view
     * @param {string} section - Name of the section.
     * @returns {void}
     */
    setActiveSectionInDOM(section) {
        $('#partsCarrousel li').removeClass('active');
        $('#partsCarrousel li[data-option="'+section+'"]').addClass('active');
        // $('#partsCarrousel li[data-option="'+section+'"]').trigger('click');
    }

    getActiveSectionInDOM() {
        let activeOption = $('#partsCarrousel li.active').attr('data-option');
        return activeOption;
    }

    /**
     * Loads a 3D model using glb or gltf files.
     * @param {string} modelPath - Filepath where the new model's files are located.
     * @param {number} cameraFar - Camera's distance from the model on the z axis.
     * @returns {void}
     */
    loadModel3D(modelPath, cameraFar) {
        let _this = this;

        // Just works when there's already a loaded model
        this.unloadModel3D();

        // We load the model
        this.loader.load(modelPath, function (gltf) {
            _this.model = gltf.scene;

            // Set the model name based on filename
            let path = modelPath.split("/");
            _this.model.name = path[path.length-1].split(".")[0];

            // console.group("Partes del modelo");
            _this.model.traverse(o => {
                if (o.isMesh) {
                    console.log("Section init: ", o.name)
                    // _this.initSection(o);

                    o.castShadow = true;
                    o.receiveShadow = true;
                    o.material.smoothShading = true;
                    // o.material.flatShading = true;
                    if(o.material.map) o.material.map.anisotropy = 16;
                    // console.log(o.name);
                }
            });
            // console.groupEnd();

            //_this.model.scale.set(1, 1, 1);
            //_this.model.rotation.y = Math.PI;

            _this.adjustSceneToObject(cameraFar);

            _this.scene.add(_this.model);
            // console.log("LOAD SCENE: ", _this.scene);

            }, undefined, function (error) {
                console.error(error);
            }
        );
    }

    // Funcion de prueba para inicializar el objeto con canvas
    initSection(section) {
        //crear canvas
        let index = this.getBufferIndex(section);

        if (index != null) {

            this.updateEmbroideries();

            // creamos canvas
            let texture = new THREE.CanvasTexture(this.buffers[index]);
            texture.flipY = false;

            // Asignamos
            section.material.map = texture;
        }
    }

    /**
     * Unloads a loaded 3D model
     * @returns {void}
     */
    unloadModel3D() {
        if (this.model) {
            // console.group("Eliminando modelo del environment");
            // console.log(`Model ${this.model.name} unloaded!`);
            this.scene.remove(this.scene.getObjectByName(this.model.name));
            this.model = null;
            // console.log("SCENE: ", this.scene);
            // console.groupEnd();
        }
    }

    /**
     * Adjusts the created scene to model's features to improve user's experience.
     * @param {number} cameraFar - Camera's distance from the model on the z axis.
     * @returns {void}
     */
    adjustSceneToObject(cameraFar) {
        let box = new THREE.Box3().setFromObject(this.model);
        let center = new THREE.Vector3();
        let hModel = new THREE.Vector3();;
        box.getCenter(center);
        this.model.position.sub(center);
        box.getSize(hModel);
        cameraFar = 4*hModel.z;
        this.camera.position.z = cameraFar;

        // console.group("Calculando position y");
        // console.log(hModel.y/2);
        // console.groupEnd();

        // Add hemisphere light to scene
        let hemiLight = new THREE.HemisphereLight(0xffffff, 0xffffff, 0.50);
        hemiLight.position.set(0, 50, 0);
        hemiLight.name = "hemiLight";
        this.scene.add(hemiLight);

        this.dirLight = new THREE.SpotLight(0xffffff, 0.5);
        this.dirLight.position.copy(this.camera.position);
        this.dirLight.castShadow = true;
        this.dirLight.shadow.bias = -0.01;
        this.dirLight.shadow.mapSize.width = 1024*4;
        this.dirLight.shadow.mapSize.height = 1024*4;
        this.dirLight.name = "dirLight";
        
        // Improving 3D visor performance and incrementing realism
        this.dirLight.shadow.mapSize.width = 2048;
        this.dirLight.shadow.mapSize.height = 2048;
        this.dirLight.shadow.camera.near = 200;
        this.dirLight.shadow.camera.far = 1500;
        this.dirLight.shadow.camera.fov = 40;
        this.dirLight.shadow.bias = - 0.005;

        this.scene.add(this.dirLight);
    }

    buildPartsCarrousel() {
        let $container = $('#partsCarrousel');
        $container.html('');

        // let currentModel = $('#carouselModels li.active').attr('data-option');
        
        let row = db['models'][this.activeModelName];
        let $li, $img, $span, $text;

        for (let i = 0; i < row['parts'].length; i++) {

            // I will create the image option just if that part accepts embroideries
            // if (row['parts'][i]['embroideries']) {
                $img = $('<span>', {
                    'class': 'img',
                    'style': 'background-image: url("models/'+this.activeModelName+'/parts/'+row['parts'][i]['name']+'.png");'
                });
                $span = $('<span>', {
                    'class': 'decoration-selected'
                });
                $li = $('<li>', {
                    'data-option': row['parts'][i]['name']
                });
                $text = $('<p>', {
                    'class': 'font-weight-bold text-secondary',
                    'style': 'letter-spacing: 2px;',
                    'text': row['parts'][i]['text'],
                });
                $li.append($img);
                $li.append($span);
                $li.append($text);
                $container.append($li);
            // }
        }
        $('#partsCarrousel li:first-child').addClass('active');
    }

    /**
     * Build texture carrousel and add listeners
     * @param {array} arr - array of textures or colors (will change).
     * @returns {void}
     */
    buildTextures(target, arr, trayClass, indexParent) {
        let cont = $(target);
        let swatches = document.querySelectorAll("." + trayClass);

        for (let swatch of swatches) {
            swatch.removeEventListener('click', function() {});
        }

        cont.html('');

        for (let [i, item] of arr.entries()) {
            let swatch = $('<div>', {
                'class': trayClass,
                'data-toggle': 'tooltip',
                'data-placement': 'top',
                'title': item['name']
            });

            if(indexParent != undefined)
                swatch.attr('data-key', indexParent + '_' + i);
            else
                swatch.attr('data-key', i);

            if (item.texture)
                swatch.css('background-image', "url(" + item.texture + ")");
            else
                swatch.css('background', "#" + item.color);

            cont.append(swatch);
        }
        if(arr.length <= 0) {
            if(indexParent == undefined)
                cont.html('Sin telas disponibles');
            else
                cont.html('Sin colores disponibles');
        }

        swatches = document.querySelectorAll("." + trayClass);

        for (let swatch of swatches) {
            swatch.addEventListener('click', this.selectSwatch.bind(this));
        }

        if(indexParent != undefined)
            $('[data-toggle="tooltip"]').tooltip({'delay': { show: 500, hide: 200 }});
    }

    // No me gusta la implementación (Comentado y codificado por Sergio)
    updateBuildTexturesAndColors() {
        let item = db['models'][this.activeModelName]['clothes'][this.getActiveSectionInDOM()];
        let key = 0;

        if(item == undefined) {
            this.buildTextures('#texturesBox', [], 'tray-parent-swatch');
            this.buildTextures('#colorsBox', [], 'tray-swatch', 0);
            return;
        }

        let buffer = this.buffers[this.getBufferIndex(this.getActiveSectionInDOM())];
        if(buffer != undefined)
            key = buffer.textureIndex;

        this.buildTextures('#texturesBox', item, 'tray-parent-swatch');
        this.buildTextures('#colorsBox', item[key]['colors'], 'tray-swatch', 0);
    }

    /**
     * Set a new material to a model's section
     * @param {string} section - The model's section that will be modified.
     * @param {THREEjs Object} material - Contains the appearance that will be set to the model's section.
     * @returns {void}
     */
    setMaterial(section, material) {
        if(this.model) {
            this.model.traverse(o => {
                if (o.isMesh) {
                    if (o.name == section) {
                        // Threejs MeshPhongMaterial
                        if (material.constructor.prototype.isMeshPhongMaterial) {
                            if (o.material != null) o.material.dispose();
                            o.material = material;
                            // console.log("1: ", section)
                        } else {
                            // Acá entra Threejs Texture - Texture canvas o lo que no es MeshPhong
                            if (o.material.map != null) o.material.map.dispose();
                            o.material.map = material;
                            // console.log("2: ", section)
                        }
                        o.material.needsUpdate = true;
                    }
                }
            });
        }
    }

    /**
     * Add controls to canvas
     * @returns {void}
     */
    addControls() {
        this.controls = new OrbitControls(this.camera, this.renderer.domElement);
        this.controls.maxPolarAngle = Math.PI / 2;
        this.controls.minPolarAngle = Math.PI / 4;
        this.controls.enableDamping = true; // We give a sense of weight to the controls
        this.controls.dampingFactor = 0.1; // The damping inertia factor
        this.controls.enablePan = false; // Camera pannig
        this.controls.autoRotate = false; // Toggle this if you'd like the chair to automatically rotate
        this.controls.autoRotateSpeed = 3; // 30
        this.controls.maxDistance = 3;
        this.controls.minDistance = 2.1;
        this.controls.name = "controls";
    }

    /**
     * Add listeners to canvas
     * @returns {void}
     */
    addListeners() {
        // Renderer listeners
        this.renderer.domElement.addEventListener('mousedown', this.mouseDown.bind(this), false);
        this.renderer.domElement.addEventListener('mouseup',   this.mouseUp.bind(this), false);
        this.renderer.domElement.addEventListener('mousemove', this.mouseMove.bind(this), false);
        this.renderer.domElement.addEventListener('keydown',   this.editText.bind(this), false);

        // Controls listeners
        this.controls.addEventListener('change', this.updateLightPosition.bind(this));
    }

    /**
     * Adjust canvas in the container (DOM)
     * @returns {void}
     */
    adjustContainerCanvasMeasures() {
        let $canvasContainer = $('#canvasContainer');
        this.renderer.setSize($canvasContainer.width(), $canvasContainer.height());
    }

    /**
     * Renders all objects included in the scene.
     * @returns {void}
     */
    render() {
        this.controls.update();
        this.renderer.render(this.scene, this.camera);

        // Resizing renderer to canvas
        if (resizeRendererToDisplaySize(this.renderer)) {
            const canvas = this.renderer.domElement;
            this.camera.aspect = canvas.clientWidth / canvas.clientHeight;
            this.camera.updateProjectionMatrix();
        }

        // Solo descomentar para presentar al cliente
        // To enable initial rotation animation
        if (this.model != null && this.loaded == false) {
            var values = initialRotation(initRotate, this.model);
            initRotate = values[0];
            this.loaded = values[1];
        }
    }

    /**
     * Edits text in a texture.
     * @param {Event} event
     * @returns {void}
     */
    editText(event) {
        // Only for testing
        if(event.key == "z") {
            // To enable or disable edition
            this.editing = !this.editing;

            if (this.selectedEmbroidery > -1 ) {
                this.embroideries[this.selectedEmbroidery].setEditing(this.editing);
                this.updateEmbroideries();

                if (!this.editing) this.selectedEmbroidery = -1;
            }

        }
        // if(event.key == "q") {
        //     // TEST
        //     this.takeImages();
        // }

        if (this.editing && this.selectedEmbroidery > -1) {
            if(event.key == "a") {
                let textSize = this.embroideries[this.selectedEmbroidery].getTextSize();
                textSize += 5;
                this.embroideries[this.selectedEmbroidery].setTextSize(textSize);
            }
            else if(event.key == "s") {
                let textSize = this.embroideries[this.selectedEmbroidery].getTextSize();
                textSize -= 5;
                this.embroideries[this.selectedEmbroidery].setTextSize(textSize);
            }
            else if(event.key == "f") {
                this.takeImages();
            }
            else if(event.key == "p") {
                // PRUEBA
            }

            this.updateEmbroideries();
        }
    }

    /**
     * Set autorotate variable.
     * @param {Boolean} autoRotate
     * @returns {void}
     */
    setAutoRotate(val) {
        this.controls.autoRotate = val;
    }

    /**
     * Update ligth position with the camera.
     * @returns {void}
     */
    updateLightPosition() {
        
        if (this.dirLight != null)
            this.dirLight.position.copy( this.camera.position );

        this.camera.updateProjectionMatrix();        
        this.camera.getWorldDirection(this.direction);
        
        this.sprite.position.copy(this.camera.position).add(
            this.direction.multiplyScalar(1)
        );
        
        this.sprite.quaternion.copy( this.camera.quaternion );
        this.sprite.lookAt(this.camera.position);
    }

    /**
     * Rotate animation.
     * @param {string} activeOption
     * @returns {void}
     */
    rotateTo(activeOption) {
        let actlpos, destpos, normal, element, transformation, angle, varangle, tween, positions;

        transformation      = new THREE.Matrix4();
        destpos             = new THREE.Vector3();
        normal              = new THREE.Vector3();
        actlpos             = new THREE.Vector3();

        // positions           = db.models.polo.parts;
        positions           = db['models'][this.activeModelName]['parts'];

        element = positions.find( function(ele) { return ele.name == activeOption });

        if (element != undefined) {

            actlpos  = this.camera.position.clone();
            destpos.set(element.x, element.y, element.z);
            normal      = normal.crossVectors(this.camera.position.clone().normalize(), destpos.normalize()).normalize();
            angle       = {value: actlpos.angleTo(destpos)};
            varangle    = {value: 0}

            function animate(time) {requestAnimationFrame(animate);TWEEN.update(time);}
            requestAnimationFrame(animate);

            tween       = new TWEEN.Tween( varangle )
            .to( {value: angle.value} , 1000 )
            .easing( TWEEN.Easing.Quadratic.Out )
            .onUpdate(
                function() {
                    env.camera.position.copy(actlpos);
                    transformation.makeRotationAxis( normal, varangle.value );
                    env.camera.applyMatrix4( transformation );
                }
            )
            .start();

            if (this.editing) {
                // We reset control variables when we change to another section
                this.handlerActivated = false;
                this.editing = false;

                for (let index = 0; index < this.embroideries.length; index++) {
                    const embroidery = this.embroideries[index];

                    if (embroidery.getEditing()) embroidery.setEditing(false);
                }

                this.updateEmbroideries();
            }

            // console.group("rotate")
            // console.log("Rotando a: ", element)
            // console.log("Valores de control: editing: ", this.editing, " handlerActivated: ", this.handlerActivated)
            // console.log("embroideries: ", this.embroideries)
            // console.groupEnd()
        }
    }

    setCameraTo(section) {
        // let positions = db.models.polo.parts;
        let positions = db['models'][this.activeModelName]['parts'];
        let element = positions.find( function(ele) { return ele.name == section });
        this.camera.position.set(element.x, element.y, element.z);
    }

    /**
     * Take a screenshot of the render.
     * @returns {void}
     */
    takeImages() {
        let a = document.createElement('a');
        this.renderer.setSize(900, 1180);
        this.renderer.render(this.scene, this.camera);
        a.href = this.renderer.domElement.toDataURL().replace("image/png", "image/octet-stream");
        a.download = 'canvas.png';
        a.click();
        this.adjustContainerCanvasMeasures();
        resizeRendererToDisplaySize(this.renderer);
    }

    // // Esto es para los datos que se deben pasar al otro equipo de la aplicación
    // takeImages() {
    //     // let activeSection = this.getActiveSectionInDOM();
    //     // let sections = ['frente', 'espalda', 'manga_izquierda', 'manga_derecha'];
    //     // for (let i = 0; i < sections.length; i++) {
    //         // this.setCameraTo(sections[i]);
    //         let a = document.createElement('a');
    //         this.renderer.setSize(720, 1280);
    //         this.renderer.render(this.scene, this.camera);
    //         a.href = this.renderer.domElement.toDataURL().replace("image/png", "image/octet-stream");
    //         a.download = 'canvas.png';
    //         a.click();
    //         // this.adjustContainerCanvasMeasures();
    //     // }
    //     // this.setCameraTo(activeSection);
    //     this.adjustContainerCanvasMeasures();
    //     resizeRendererToDisplaySize(this.renderer);
    // }

    /**
     * Save a file.
     * @param {string} strData - I dont know
     * @param {string} filename - I dont know
     * @returns {void}
     */
    saveFile(strData, filename) {
        var link = document.createElement('a');
        if (typeof link.download === 'string') {
            document.body.appendChild(link); //Firefox requires the link to be in the body
            link.download = filename;
            link.href = strData;
            link.click();
            document.body.removeChild(link); //remove the link when done
        } else {
            location.replace(uri);
        }
    }

    /**
     * Event - Change Material.
     * @param {event} e
     * @returns {void}
     */
    selectSwatch(e) {
        let index = null;
        let key = (e.target.dataset.key).split('_');
        // let item = db.clothes[this.getActiveSectionInDOM()][parseInt(key[0])];
        let item = db['models'][this.activeModelName].clothes[this.getActiveSectionInDOM()][parseInt(key[0])];

        if(key.length > 1) {
            item = item['colors'][parseInt(key[1])];
        }

        if (item.texture) {

            // if(this.getActiveSectionInDOM() == 'manga_izquierda' || this.getActiveSectionInDOM() == 'manga_derecha') {
            if(this.getActiveSectionInDOM() == 'mangas') {
                index = this.getBufferIndex('manga_izquierda');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

                index = this.getBufferIndex('manga_derecha');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

            } else if(this.getActiveSectionInDOM() == 'base_mangas') {
            //   else if(this.getActiveSectionInDOM() == 'basemanga_izquierda' || this.getActiveSectionInDOM() == 'basemanga_derecha') {
                
                index = this.getBufferIndex('basemanga_izquierda');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

                index = this.getBufferIndex('basemanga_derecha');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

            } else if(this.getActiveSectionInDOM() == 'puños') {
            //   else if(this.getActiveSectionInDOM() == 'basemanga_izquierda' || this.getActiveSectionInDOM() == 'basemanga_derecha') {
                
                index = this.getBufferIndex('puño_derecha');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

                index = this.getBufferIndex('puño_izquierda');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

            } else if(this.getActiveSectionInDOM() == 'perilla_puños') {
            //   else if(this.getActiveSectionInDOM() == 'basemanga_izquierda' || this.getActiveSectionInDOM() == 'basemanga_derecha') {
                
                index = this.getBufferIndex('perillapuño_derecha');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

                index = this.getBufferIndex('perillapuño_izquierda');
                this.buffers[index].back.src = item.texture;
                this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                this.buffers[index].color = "#"+item.colors[0].color;
                this.buffers[index].textureIndex = parseInt(key[0]);

            } else {
                let index = this.getBufferIndex(this.getActiveSectionInDOM());

                if (index != null) {
                    this.buffers[index].back.src = item.texture;
                    this.buildTextures('#colorsBox', item['colors'], 'tray-swatch', parseInt(key[0]));
                    this.buffers[index].color = "#"+item.colors[0].color;
                    this.buffers[index].textureIndex = parseInt(key[0]);
                }
            }
        } else {

            // When I'll change the color of the previously selected texture
            if(this.getActiveSectionInDOM() == 'mangas') {
            
            // if(this.getActiveSectionInDOM() == 'manga_izquierda' || this.getActiveSectionInDOM() == 'manga_derecha') {
                index = this.getBufferIndex('manga_izquierda');
                this.buffers[index].color = "#"+item.color;

                index = this.getBufferIndex('manga_derecha');
                this.buffers[index].color = "#"+item.color;

                this.updateEmbroideries('manga_izquierda');
                this.updateEmbroideries('manga_derecha');
            
            } else if(this.getActiveSectionInDOM() == 'base_mangas') {
            //   else if(this.getActiveSectionInDOM() == 'basemanga_izquierda' || this.getActiveSectionInDOM() == 'basemanga_derecha') {
                
                index = this.getBufferIndex('basemanga_izquierda');
                this.buffers[index].color = "#"+item.color;

                index = this.getBufferIndex('basemanga_derecha');
                this.buffers[index].color = "#"+item.color;

                this.updateEmbroideries('basemanga_izquierda');
                this.updateEmbroideries('basemanga_derecha');
            
            } else if(this.getActiveSectionInDOM() == 'puños') {
            //   else if(this.getActiveSectionInDOM() == 'basemanga_izquierda' || this.getActiveSectionInDOM() == 'basemanga_derecha') {
                
                index = this.getBufferIndex('puño_derecha');
                this.buffers[index].color = "#"+item.color;

                index = this.getBufferIndex('puño_izquierda');
                this.buffers[index].color = "#"+item.color;

                this.updateEmbroideries('puño_izquierda');
                this.updateEmbroideries('puño_derecha');
            
            } else if(this.getActiveSectionInDOM() == 'perilla_puños') {
            //   else if(this.getActiveSectionInDOM() == 'basemanga_izquierda' || this.getActiveSectionInDOM() == 'basemanga_derecha') {
                
                index = this.getBufferIndex('perillapuño_derecha');
                this.buffers[index].color = "#"+item.color;

                index = this.getBufferIndex('perillapuño_izquierda');
                this.buffers[index].color = "#"+item.color;

                this.updateEmbroideries('perillapuño_izquierda');
                this.updateEmbroideries('perillapuño_derecha');
            
            } else {
                index = this.getBufferIndex(this.getActiveSectionInDOM());
                this.buffers[index].color = "#"+item.color;
            }
        }
        
        this.updateEmbroideries();
    }

    /**
     * Raycast used for mouse picking amongst other things (Objects in the 3D space the mouse is over).
     * @param {Event} event - A client event object taken from the navigator.
     * @returns {void}
     */
    mouseMove(event) {

        if(this.moving) {
            let pos = this.canvas.getBoundingClientRect();
            this.mouse.x = ( (event.clientX - pos.left) / pos.width ) * 2 - 1;
            this.mouse.y = - ( (event.clientY - pos.top) / pos.height ) * 2 + 1;

            this.raycaster.setFromCamera( this.mouse, this.camera );

            const intersects = this.raycaster.intersectObjects( this.scene.children, true );

            if(intersects.length > 1) {
                if(this.selectedEmbroidery != -1) {
                    if(this.embroideries[this.selectedEmbroidery].getSection() == intersects[this.intersectIndex].object.name) {
                        this.mouseText.x = intersects[this.intersectIndex].uv.x;
                        this.mouseText.y = intersects[this.intersectIndex].uv.y;

                        if(this.mouseMoveCont == 0) {
                            // the first mouseMouve
                            this.mouseMoveCont += 1
                            this.mouseText.x2 = this.mouseText.x;
                            this.mouseText.y2 = this.mouseText.y;

                        } else {
                            // When we are already moving

                            this.setActiveSectionInDOM(intersects[this.intersectIndex].object.name);
                            // console.log(`TextStamp: POSU: ${this.embroideries[this.selectedEmbroidery].getPosU()}, POSV: ${this.embroideries[this.selectedEmbroidery].getPosV()}`);
                            // console.log("MOUSE FIRST: ", this.mouseFirst);
                            // console.log("MOUSE TEXT: ", this.mouseText);

                            let diferenceX = (this.mouseText.x - this.mouseText.x2);
                            let diferenceY = (this.mouseText.y - this.mouseText.y2);

                            // console.log(`DIFERENCE X: ${diferenceX} - Diference Y: ${diferenceY}`);
                            this.mouseText.x2 += diferenceX;
                            this.mouseText.y2 += diferenceY;

                            this.embroideries[this.selectedEmbroidery].setPos(this.embroideries[this.selectedEmbroidery].getPosU() + diferenceX, this.embroideries[this.selectedEmbroidery].getPosV() + diferenceY);
                            this.updateEmbroideries();
                        }
                    }
                }
            }
            event.stopImmediatePropagation();
        }

        if (this.selectedEmbroidery > -1 && this.editing) {
            //Zoom handler
            let zoomState = this.embroideries[this.selectedEmbroidery].handlers[2].getZoomState();
            let rotateState = this.embroideries[this.selectedEmbroidery].handlers[3].getRotateState();

            if (zoomState || rotateState) {
                let pos = this.canvas.getBoundingClientRect();
                this.mouse.x = ( (event.clientX - pos.left) / pos.width ) * 2 - 1;
                this.mouse.y = - ( (event.clientY - pos.top) / pos.height ) * 2 + 1;

                this.raycaster.setFromCamera( this.mouse, this.camera );

                const intersects = this.raycaster.intersectObjects( this.scene.children, true );

                if(intersects.length > 1) {
                    if(this.embroideries[this.selectedEmbroidery].getSection() == intersects[this.intersectIndex].object.name) {
                        let embroidery = this.embroideries[this.selectedEmbroidery];
                        let backIndex = this.getBufferIndex(embroidery.getSection());

                        this.mouse.x = intersects[this.intersectIndex].uv.x * this.buffers[backIndex].back.width;
                        this.mouse.y = intersects[this.intersectIndex].uv.y * this.buffers[backIndex].back.height;

                        let tx = embroidery.getPosU() * this.buffers[backIndex].back.width;
                        let ty = embroidery.getPosV() * this.buffers[backIndex].back.height;

                        if(zoomState) {
                            let diferenceX = Math.abs(this.mouse.x - tx);
                            let diferenceY = Math.abs(this.mouse.y - ty);

                            // console.log(`x: ${this.mouse.x} y: ${this.mouse.y} tx: ${tx} ty: ${ty}`);

                            // console.log("Scala x: ", diferenceX);
                            // console.log("Scala y: ", diferenceY);

                            embroidery.handlers[2].changeScalingFactor(diferenceX, diferenceY);
                        } else if(rotateState) {
                            if(this.prevMouse.x != undefined) {
                                let dx = this.mouse.x - tx;
                                let dy = this.mouse.y - ty;
                                let angle = Math.atan2(dy, dx);

                                if(angle < 0)
                                    angle = ((2*Math.PI) + angle);

                                dx = this.prevMouse.x - tx;
                                dy = this.prevMouse.y - ty;

                                angle -= Math.atan2(dy, dx);
                                if(angle < 0)
                                    angle = ((2*Math.PI) + angle);

                                // console.log(angle * 180/Math.PI);
                                embroidery.handlers[3].dynamicRotate(angle * 180/Math.PI);
                            } else {
                                this.prevMouse.x = this.mouse.x;
                                this.prevMouse.y = this.mouse.y;
                            }
                        }
                        this.updateEmbroideries(embroidery.getSection());
                    }
                }
                event.stopImmediatePropagation();
            }
        }
    }

    /**
     * Event - Mouse up.
     * @param {event} event
     * @returns {void}
     */
    mouseUp(event) {
        if (this.selectedEmbroidery > -1) {
            // Set moving stamp as false because moveUp got activated
            this.moving = false;

            // Restart last position
            this.mouseMoveCont = 0;
            this.mouseText.x2 = 0;
            this.mouseText.y2 = 0;

            this.embroideries[this.selectedEmbroidery].setEditing(this.editing);

            if (this.embroideries[this.selectedEmbroidery].handlers[2].getZoomState()) {
                this.embroideries[this.selectedEmbroidery].handlers[2].changeZoomState();
                this.updateEmbroideries();
            }
            if (this.embroideries[this.selectedEmbroidery].handlers[3].getRotateState()) {
                this.embroideries[this.selectedEmbroidery].handlers[3].changeRotateState();
                this.updateEmbroideries();
            }
        }
        this.prevMouse.x = undefined;
        this.prevMouse.y = undefined;
    }

    /**
     * Event - Mouse down.
     * @param {event} event
     * @returns {void}
     */
    mouseDown(event) {
        switch ( event.which ) {
            case 1: // Left
                this.findEdit(event);
                this.putPlaceholderStamp();
                break;
            case 3: // Right
                break;

            default:
                console.log("This option is not available!");
                break;
        }
    }

    /**
     * It's a raycaster
     * @param {event} event
     * @returns {void}
     */
    findEdit(event) {
        if (this.embroideries.length && !this.isPlaceholderStamp) {
            let pos = this.canvas.getBoundingClientRect();

            this.mouse.x = ( (event.clientX - pos.left) / pos.width ) * 2 - 1;
            this.mouse.y = - ( (event.clientY - pos.top) / pos.height ) * 2 + 1;

            this.raycaster.setFromCamera( this.mouse, this.camera );
            let intersects = this.raycaster.intersectObjects( this.scene.children, true );

            if( intersects.length > 1 ) {
                this.mouseText.x = intersects[this.intersectIndex].uv.x;
                this.mouseText.y = intersects[this.intersectIndex].uv.y;

                let section = intersects[this.intersectIndex]['object'];
                if(section != undefined)
                    section = section['name'];

                let values = this.isClickIn(this.mouseText.x, this.mouseText.y, section);

                // Para cuando cambie de una estampa a otra
                if (this.selectedEmbroidery > -1 && this.selectedEmbroidery != values[1]) {
                    this.embroideries[this.selectedEmbroidery].setEditing(false);
                    this.updateEmbroideries();
                }

                this.editing = values[0];

                // New selected stamp pos
                this.selectedEmbroidery = values[1];

                // Para pintar manejadores cuando se este modificando una estampa
                if (this.selectedEmbroidery > -1) {
                    this.moving = this.getMovingState();
                    this.embroideries[this.selectedEmbroidery].setEditing(this.editing);
                    this.updateEmbroideries();
                    this.updateEmbroideries(this.embroideries[this.selectedEmbroidery].getSection());
                    this.setActiveSectionInDOM(this.embroideries[this.selectedEmbroidery].getSection());
                    this.updateBuildTexturesAndColors();
                    // let newTexture = this.embroideries[this.selectedEmbroidery].generateTextStamp(this.back, this.manejadores, this.embroideries[this.selectedEmbroidery].getPosU(), this.embroideries[this.selectedEmbroidery].getPosV());
                    // this.setMaterial('frente', newTexture);
                }

                // Colocaría el this.updateEmbroideries() acá pero se pintarían las estampas
                // incluso cuando no se seleccioné una estampa y no nos interesa eso
                // porque se pintarían de nuevo cuando no tenemos que
            }
        }
    }

    getMovingState() {
        // Debe estarse editando y no debe estar fixed activado
        let fixedState = this.embroideries[this.selectedEmbroidery].handlers[1].getFixed();
        let zoomState = this.embroideries[this.selectedEmbroidery].handlers[2].getZoomState();
        let rotateState = this.embroideries[this.selectedEmbroidery].handlers[3].getRotateState();

        return (this.editing && !fixedState && !zoomState && !rotateState);
    }

    findHandlers(embroidery, x, y, width, height, backIndex, index) {
        let handlers = embroidery.getHandlers();
        let indexHandler = 0;
        let checkHandler = false;

        for (indexHandler = 0; indexHandler < handlers.length; indexHandler++) {
            if(handlers[indexHandler].isClickIn(x, y, embroidery.getPosU(), embroidery.getPosV(), this.buffers[backIndex].back, width, height)) {
                checkHandler = true;
                break;
            }
        }

        // Si entro, set handlerActivated como true sino false para que se deje de dibujar
        if (checkHandler) {
            this.handlerActivated = true;
            let handlerType = handlers[indexHandler].getHandlerType();

            // console.log("HandlerType: ", handlerType);

            switch(handlerType) {
                case 'delete':
                    this.removeEmbroidery(index);
                    index = -1;
                    this.handlerActivated = false;
                    break;

                case 'zoom':
                    handlers[indexHandler].changeZoomState();
                    break;

                case 'rotate':
                    // handlers[indexHandler].rotate(30);
                    handlers[indexHandler].changeRotateState();
                    break;

                case 'fixed':
                    handlers[indexHandler].fixedAction();
                    break;

                case 'edit':
                    handlers[indexHandler].editAction(this.selectedEmbroidery);
                    break;

                default:
                    console.log("It's not an available handler!");
                    break;
            }

        } else {
            this.handlerActivated = false;
        }
    }

    /**
     * Find the clicked stamp.
     * @param {number} U - Position U (U,V)
     * @param {number} V - Position V (U,V)
     * @returns {Array}
     */
    isClickIn(U, V, section) {
        // Se requiere el tamaño de la textura de cada elemento para determinar los valores relativos
        let inside = false;
        let selectedIndex = -1;
        let backIndex = -1;

        if(section == undefined)
            return [inside, selectedIndex];

        for (let index = 0; index < this.embroideries.length; index++) {
            let embroidery = this.embroideries[index];
            if(!embroidery.getVisible()) continue;
            if(embroidery.getSection() != section) continue;

            // We check we are in the active section
            // if (embroidery.getSection() == this.getActiveSectionInDOM()) {
                backIndex = this.getBufferIndex(embroidery.getSection());

                if (backIndex != -1) {
                    let x = U * this.buffers[backIndex].back.width;
                    let y = V * this.buffers[backIndex].back.height;

                    let tx = embroidery.getPosU() * this.buffers[backIndex].back.width;
                    let ty = embroidery.getPosV() * this.buffers[backIndex].back.height;

                    // let width = embroidery.getWidthBox();
                    // let height = embroidery.getHeightBox();
                    let width = embroidery.boxEdit.width - (embroidery.iconSize * 2);
                    let height = embroidery.boxEdit.height - (embroidery.iconSize * 2);

                    // if para manejadores
                    if (this.editing && this.selectedEmbroidery == index) {
                        this.findHandlers(embroidery, x, y, width, height, backIndex, index);
                        if (this.handlerActivated) return [true, index];
                    }

                    // Para encontrar el texto
                    if ( (x >= tx - width/2 && x <= tx + width/2) && (y >= ty - height/2 && y <= ty + height/2) ) {
                        selectedIndex = index;
                        inside = true;
                        break;
                        // console.log(`selected: ${this.selectedEmbroidery} - inside: ${inside}`);
                    }

                    backIndex = -1;
                }
            // }
        }

        return [inside, selectedIndex];
    }

    newPlaceholderStamp(section, data, callback) {
        this.isPlaceholderStamp = true;
        console.log("section: ", section);

        if(data['img'] == undefined) {
            this.placeholderStamp = {
                'section': section,
                'textFont': data['textFont'],
                'textContent': data['textContent'],
                'textColor': data['textColor'],
            };
        } else {
            this.placeholderStamp = {
                'section': section,
                'img': data['img'],
            };
        }
        this.placeholderStampCallback = callback;
        return this.embroideries.length;
    }

    putPlaceholderStamp() {
        if(this.isPlaceholderStamp) {
            let pos = this.canvas.getBoundingClientRect();

            this.mouse.x = ( (event.clientX - pos.left) / pos.width ) * 2 - 1;
            this.mouse.y = - ( (event.clientY - pos.top) / pos.height ) * 2 + 1;

            this.raycaster.setFromCamera( this.mouse, this.camera );
            let intersects = this.raycaster.intersectObjects( this.scene.children, true );

            if( intersects.length > 1 ) {
                
                let sectionName = intersects[this.intersectIndex]['object']['userData']['name'];

                if (getPartEmbroideryAvailability(db['models'][this.activeModelName]['parts'], sectionName)) {
                    this.placeholderStamp['section'] = sectionName;
                    this.updateEmbroideries();
                    this.placeholderStamp['posU'] = intersects[this.intersectIndex].uv.x;
                    this.placeholderStamp['posV'] = intersects[this.intersectIndex].uv.y;
                    if(this.placeholderStamp['img'] == undefined)
                        this.addTextEmbroidery(this.placeholderStamp);
                    else
                        this.addImageEmbroidery(this.placeholderStamp);
                    this.setActiveSectionInDOM(this.placeholderStamp['section']);
                    this.updateBuildTexturesAndColors();
                    this.updateEmbroideries(this.placeholderStamp['section']);
                    if(this.placeholderStampCallback)
                        this.placeholderStampCallback(false);
                    this.clearPlaceholderStamp();
                } else {
                    notify('warning', "Lo sentimos, esta sección de la prenda no esta recibiendo bordados por el momento.");
                }
                
            } else {
                notify('warning', "Debe darle click a una parte válida de la prenda");
            }
        }
    }

    clearPlaceholderStamp() {
        this.isPlaceholderStamp = false;
        this.placeholderStamp = [];
        this.placeholderStampCallback = null;
    }

    /**
     * Add a text stamp to the model
     * @param {string} section - section's name
     * @param {string} textFont - font family
     * @param {string} textContent - the message
     * @param {string} textColor - the color
     * @returns {void}
     */
    addTextEmbroidery(section, textFont, textContent, textColor="#000000") {
        let newTextEmbroidery;
        let posU = 0.5;
        let posV = 0.5;

        if(typeof section != 'string') {
            section = this.placeholderStamp['section'];
            textFont = this.placeholderStamp['textFont'];
            textContent = this.placeholderStamp['textContent'];
            textColor = this.placeholderStamp['textColor'];
            posU = this.placeholderStamp['posU'];
            posV = this.placeholderStamp['posV'];
        }

        newTextEmbroidery = new TextEmbroidery(
            'polo',
            section,
            120,
            textFont,
            textContent,
            textColor,
            this.handlerImages,
            posU,
            posV
        );

        this.embroideries.push(newTextEmbroidery);
        this.updateEmbroideries();
        return (this.embroideries.length-1);
    }

    /**
     * Add a text stamp to the model
     * @param {string} section - section's name
     * @param {Image} img - image
     * @returns {void}
     */
    addImageEmbroidery(section, img) {
        let newImageEmbroidery;
        let posU = 0.5;
        let posV = 0.5;

        if(typeof section != 'string') {
            section = this.placeholderStamp['section'];
            img = this.placeholderStamp['img'];
            posU = this.placeholderStamp['posU'];
            posV = this.placeholderStamp['posV'];
        }

        newImageEmbroidery = new ImageEmbroidery(
            'polo',
            section,
            img,
            this.handlerImages,
            posU,
            posV
        );

        this.embroideries.push(newImageEmbroidery);
        this.updateEmbroideries();
        return (this.embroideries.length-1);
    }

    updateEmbroidery(index, data) {
        if (index > -1 && index < this.embroideries.length) this.embroideries[index].update(data);
        else throw new RangeError("Check if the value index is inside embroideries boundings.");
        this.embroideries[index].setEditing(false);
        this.updateEmbroideries(this.embroideries[index].getSection());
        this.selectedEmbroidery = -1;
    }

    removeEmbroidery(pos) {
        if (pos > -1 && pos < this.embroideries.length) {}
        else throw new RangeError("Check if the value pos is inside embroideries boundings.");
        let section = this.embroideries[pos].getSection();
        this.embroideries.splice(pos, 1)
        this.updateEmbroideries(section);
        this.selectedEmbroidery = -1;

        // Update DOM
        let index = pos;
        $('.card[data-index='+index+']').remove();
        index = parseInt(index) + 1;
        while(true) {
            let aux = $('.card[data-index='+index+']');
            if(aux.length == 0)
                break;
            aux.attr('data-index', (index-1));
            index += 1;
        }
    }

    getSelectedEmbroidery() {
        if (this.selectedEmbroidery > -1) {
            return this.embroideries[this.selectedEmbroidery];
        } else {
            return null;
        }
    }

    setSelectedEmbroidery(index) {
        if(this.selectedEmbroidery != -1) {
            this.embroideries[this.selectedEmbroidery].setEditing(false);
        }
        this.selectedEmbroidery = index;
    }

    getEmbroideries() {
        return this.embroideries;
    }

    dispose() {
        this.scene.dispose();

        this.renderer.domElement.removeEventListener('mousedown', this.mouseDown.bind(this));
        this.renderer.domElement.removeEventListener('mouseup',   this.mouseUp.bind(this));
        this.renderer.domElement.removeEventListener('mousemove', this.mouseMove.bind(this));
        this.renderer.domElement.removeEventListener('keydown',   this.editText.bind(this));

        this.renderer.renderLists.dispose();
        this.renderer.dispose();

        this.camera.remove();
        this.controls.dispose();
    }
}